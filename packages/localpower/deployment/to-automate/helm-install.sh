#!/bin/bash

# istioctl install --set profile=demo -y
# helm repo add hashicorp https://helm.releases.hashicorp.com

kubectl create ns vault || true
helm install -n vault \
  -f envs/local/vault.values.yaml \
  vault hashicorp/vault
  #-f envs/local/vault.secrets.yaml \

# After installing needs to unseal with keys from keybase
# kubectl port-forward -n vault --address 0.0.0.0 vault-0 8200:8200
# Also need to update the k8s access token in vault dashboard if the vault service account was recreated, using the new service account token

# Might need these env vars
# export VAULT_ADDR=http://127.0.0.1:8200
# export VAULT_TOKEN={token}
# export VAULT_TOKEN=$(vault write -format=json auth/kubernetes/login role=localpower-local jwt={k8s token} | jq -r ".auth.client_token")
