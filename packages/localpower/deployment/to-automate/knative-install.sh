#!/bin/bash

# Knative version 0.25.x for k8s 1.19
# Knative version 0.26.x for k8s 1.20
kubectl apply -f https://github.com/knative/operator/releases/download/v0.25.3/operator.yaml
