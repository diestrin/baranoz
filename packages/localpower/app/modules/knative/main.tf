resource "kubernetes_namespace" "istio_system" {
  provider = kubernetes
  metadata {
    name = "istio-system"
  }
}

resource "local_file" "istio-config" {
  content = templatefile("${path.module}/istio.tmpl", { })
  filename = ".istio/istio.yaml"
}

resource "null_resource" "istio" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = "istioctl install -f \".istio/istio.yaml\""
  }
  depends_on = [local_file.istio-config]
}
