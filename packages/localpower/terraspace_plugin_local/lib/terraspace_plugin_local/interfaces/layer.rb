module TerraspacePluginLocal::Interfaces
  class Layer
    extend Memoist

    # interface method
    def namespace
      "minikube"
    end

    # interface method
    def region
      "local"
    end

    # interface method
    def provider
      "local"
    end
  end
end
