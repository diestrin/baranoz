# This is where you define variable substitions for the Terraspace expander.
# Methods are available as variables.  For example:
#
#    variable | method
#    ---------|--------
#    :ACCOUNT | account
#    :REGION  | region
#
module TerraspacePluginLocal::Interfaces
  class Expander
    include Terraspace::Plugin::Expander::Interface
    def account
      "minikube"
    end

    def region
      "local"
    end

    alias_method :namespace, :account
    alias_method :location, :region
  end
end
