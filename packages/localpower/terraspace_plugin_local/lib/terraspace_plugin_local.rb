lib = File.expand_path("../../../", __FILE__)
$:.unshift(lib)

require "memoist"
require "terraspace" # for interface

require "terraspace_plugin_local/version"
require "terraspace_plugin_local/autoloader"
TerraspacePluginLocal::Autoloader.setup

module TerraspacePluginLocal
  class Error < StandardError; end

  # Friendlier method for config/plugins/local.rb. Example:
  #
  #     TerraspacePluginLocal.configure do |config|
  #       config.resource.property = "value"
  #     end
  #
  def configure(&block)
    Interfaces::Config.instance.configure(&block)
  end

  def config
    Interfaces::Config.instance.config
  end

  extend self
end

Terraspace::Plugin.register("local",
  backend: "PROVIDER_BACKEND",
  config_class: TerraspacePluginLocal::Interfaces::Config,
  layer_class: TerraspacePluginLocal::Interfaces::Layer,
  root: File.dirname(__dir__),
)
