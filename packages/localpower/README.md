# Local Power

URL: `*.localpower.diegobarahona.com`

Uses Cloudflare to route traffic from `*.clocalpower.diegobarahona.com` with a CNAME to NO-IP service, pointing to home servers.

Uses a Docker Desktop K8S deployment with Istio and Knative for eventing and serving

This is a Terraspace project. It contains code to provision Cloud infrastructure built with [Terraform](https://www.terraform.io/) and the [Terraspace Framework](https://terraspace.cloud/).

## Deploy

To deploy all the infrastructure stacks:

    terraspace all up

To deploy individual stacks:

    terraspace up demo # where demo is app/stacks/demo

## Terrafile

To use more modules, add them to the [Terrafile](https://terraspace.cloud/docs/terrafile/).
