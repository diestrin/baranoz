# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.1-alpha.4](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-localpower@0.1.1-alpha.3...@diestrin/baranoz-localpower@0.1.1-alpha.4) (2022-05-16)

**Note:** Version bump only for package @diestrin/baranoz-localpower





## [0.1.1-alpha.3](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-localpower@0.1.1-alpha.2...@diestrin/baranoz-localpower@0.1.1-alpha.3) (2022-02-11)

**Note:** Version bump only for package @diestrin/baranoz-localpower





## [0.1.1-alpha.2](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-localpower@0.1.1-alpha.1...@diestrin/baranoz-localpower@0.1.1-alpha.2) (2022-02-09)


### Bug Fixes

* update istio install config ([6198592](https://gitlab.com/diestrin/baranoz/commit/6198592a0b71a8deffb3626f347b005098b2dc18))





## [0.1.1-alpha.1](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-localpower@0.1.1-alpha.0...@diestrin/baranoz-localpower@0.1.1-alpha.1) (2021-11-14)

**Note:** Version bump only for package @diestrin/baranoz-localpower





## [0.1.1-alpha.0](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-localpower@0.1.0...@diestrin/baranoz-localpower@0.1.1-alpha.0) (2021-11-14)

**Note:** Version bump only for package @diestrin/baranoz-localpower





# [0.1.0](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-localpower@0.1.0-alpha.0...@diestrin/baranoz-localpower@0.1.0) (2021-10-24)

**Note:** Version bump only for package @diestrin/baranoz-localpower





# 0.1.0-alpha.0 (2021-10-24)


### Features

* add base ping event ([a696471](https://gitlab.com/diestrin/baranoz/commit/a6964713b710def70ede838b604cbc347ab4d1d0))
* add infrastructure project ([a5986b4](https://gitlab.com/diestrin/baranoz/commit/a5986b42298842eaa5e453a8a22c5d95f0680ec8))
* add notion-watchers chart and basic deployment ([da06647](https://gitlab.com/diestrin/baranoz/commit/da0664766cc1ccd6c056f1d16194cfd459595ea0))
