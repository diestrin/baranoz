# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.2.0-alpha.16](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.15...@diestrin/baranoz-notion-watchers@0.2.0-alpha.16) (2023-09-18)


### Features

* bac worker update ([fc215c5](https://gitlab.com/diestrin/baranoz/commit/fc215c5ebc76fff5f04648390dfd6631384eae9f))





# [0.2.0-alpha.15](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.14...@diestrin/baranoz-notion-watchers@0.2.0-alpha.15) (2022-07-17)


### Features

* clone the transaction task with no txs ([a97a776](https://gitlab.com/diestrin/baranoz/commit/a97a7762a287af9938dddcab676bf57bac127060))





# [0.2.0-alpha.14](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.13...@diestrin/baranoz-notion-watchers@0.2.0-alpha.14) (2022-07-17)


### Bug Fixes

* correct the transaction category selection ([7da303c](https://gitlab.com/diestrin/baranoz/commit/7da303cb0f7fdbb5b781be80fe6702d09745183b))





# [0.2.0-alpha.13](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.12...@diestrin/baranoz-notion-watchers@0.2.0-alpha.13) (2022-06-26)


### Bug Fixes

* update logs for single param ([2049f99](https://gitlab.com/diestrin/baranoz/commit/2049f990cdc4b8cc6130ceccef33618054666b14))





# [0.2.0-alpha.12](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.11...@diestrin/baranoz-notion-watchers@0.2.0-alpha.12) (2022-06-19)


### Bug Fixes

* include test env in jest command ([138f844](https://gitlab.com/diestrin/baranoz/commit/138f844128f6cfbbeda885557093f94080f21fed))





# [0.2.0-alpha.11](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.10...@diestrin/baranoz-notion-watchers@0.2.0-alpha.11) (2022-05-16)


### Features

* add sentry monitoring ([3650180](https://gitlab.com/diestrin/baranoz/commit/3650180ad18e9b53d060e1350589abf8105a7247))





# [0.2.0-alpha.10](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.9...@diestrin/baranoz-notion-watchers@0.2.0-alpha.10) (2022-05-16)

**Note:** Version bump only for package @diestrin/baranoz-notion-watchers





# [0.2.0-alpha.9](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.8...@diestrin/baranoz-notion-watchers@0.2.0-alpha.9) (2022-03-27)


### Features

* add support for recurrent by weekday and unit ([d687f2b](https://gitlab.com/diestrin/baranoz/commit/d687f2b17e3815c1b77f80f1fc92518d03aed6e4))





# [0.2.0-alpha.8](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.7...@diestrin/baranoz-notion-watchers@0.2.0-alpha.8) (2022-03-23)

**Note:** Version bump only for package @diestrin/baranoz-notion-watchers





# [0.2.0-alpha.7](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.6...@diestrin/baranoz-notion-watchers@0.2.0-alpha.7) (2022-02-28)


### Bug Fixes

* clean all checkboxes regardless the action ([8fca9c7](https://gitlab.com/diestrin/baranoz/commit/8fca9c7dc0561d41f08847eca910f42e67629af4))





# [0.2.0-alpha.6](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.5...@diestrin/baranoz-notion-watchers@0.2.0-alpha.6) (2022-02-26)


### Bug Fixes

* cannot assign to bots ([c3c8536](https://gitlab.com/diestrin/baranoz/commit/c3c8536b680d5ded8bea204e302d96bacb291676))





# [0.2.0-alpha.5](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.4...@diestrin/baranoz-notion-watchers@0.2.0-alpha.5) (2022-02-25)


### Bug Fixes

* cannot assign to bots ([a12a1af](https://gitlab.com/diestrin/baranoz/commit/a12a1afb1eecdd20161c0de728ac84317c41edb7))





# [0.2.0-alpha.4](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.3...@diestrin/baranoz-notion-watchers@0.2.0-alpha.4) (2022-02-18)


### Features

* implement skip and snooze for notion-watchers ([f4ee231](https://gitlab.com/diestrin/baranoz/commit/f4ee2313b63ea5f966b876251f43d010b42e4109))





# [0.2.0-alpha.3](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.2...@diestrin/baranoz-notion-watchers@0.2.0-alpha.3) (2022-02-11)


### Bug Fixes

* tolerate timeless dates ([2888f5e](https://gitlab.com/diestrin/baranoz/commit/2888f5e1a89be7f107aa751cdc144a79ad815885))





# [0.2.0-alpha.2](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.1...@diestrin/baranoz-notion-watchers@0.2.0-alpha.2) (2022-02-11)

**Note:** Version bump only for package @diestrin/baranoz-notion-watchers





# [0.2.0-alpha.1](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.2.0-alpha.0...@diestrin/baranoz-notion-watchers@0.2.0-alpha.1) (2021-11-14)

**Note:** Version bump only for package @diestrin/baranoz-notion-watchers





# [0.2.0-alpha.0](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.1.0...@diestrin/baranoz-notion-watchers@0.2.0-alpha.0) (2021-11-14)


### Bug Fixes

* create tasks with days greater than today ([c7f48e6](https://gitlab.com/diestrin/baranoz/commit/c7f48e666161cea13eeaea3487bc9fe2c2f0e6a9))


### Features

* vault integration with notion-watchers ([b361dcc](https://gitlab.com/diestrin/baranoz/commit/b361dccc7f3b682e20aaff4282b0a49e1270f43c))





# [0.1.0](https://gitlab.com/diestrin/baranoz/compare/@diestrin/baranoz-notion-watchers@0.1.0-alpha.0...@diestrin/baranoz-notion-watchers@0.1.0) (2021-10-24)

**Note:** Version bump only for package @diestrin/baranoz-notion-watchers





# 0.1.0-alpha.0 (2021-10-24)


### Bug Fixes

* remove log in event received ([9d92a4e](https://gitlab.com/diestrin/baranoz/commit/9d92a4efbfde6e503cb16400ba1211a2760e93d3))


### Features

* add base ping event ([a696471](https://gitlab.com/diestrin/baranoz/commit/a6964713b710def70ede838b604cbc347ab4d1d0))
* add nestjs project for notion-watchers ([f536490](https://gitlab.com/diestrin/baranoz/commit/f5364909a22b647c88aea707119aef828bc7e9bf))
* add notion-watchers chart and basic deployment ([da06647](https://gitlab.com/diestrin/baranoz/commit/da0664766cc1ccd6c056f1d16194cfd459595ea0))
* add notion-watchers project base ([d6bd0b7](https://gitlab.com/diestrin/baranoz/commit/d6bd0b7298474f229f50ac845ab17ccc1d94f110))
* base knative eventing working with api ([d149243](https://gitlab.com/diestrin/baranoz/commit/d149243c7c24e91c11166c14223cb15374a5bdec))
* notion-watchers recurrence working ([7cc9703](https://gitlab.com/diestrin/baranoz/commit/7cc9703aec87d83c4f409950f41bad6db5209b42))
