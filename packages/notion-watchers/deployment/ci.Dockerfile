FROM node:lts-alpine as CI

ARG NPM_TOKEN
ARG PACKAGE_VERSION

RUN echo "@diestrin:registry=https://gitlab.com/api/v4/packages/npm/" > $HOME/.npmrc && \
    echo "//gitlab.com/api/v4/packages/npm/:_authToken=${NPM_TOKEN}" >> $HOME/.npmrc && \
    apk add bash
RUN npm i -g @diestrin/baranoz-notion-watchers@${PACKAGE_VERSION} && rm $HOME/.npmrc

CMD ["/bin/bash", "-c", "source /vault/secrets/env; node /usr/local/lib/node_modules/@diestrin/baranoz-notion-watchers"]
