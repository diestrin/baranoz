# DEV image

FROM node:lts-alpine as DEV

WORKDIR /usr/local/app

RUN apk add bash

ADD package*.json ./

RUN npm i

ADD tsconfig.json tsconfig.build.json pm2.config.json ./
ADD ./src ./src

RUN npm run build

CMD ["/bin/bash", "-c", "source /vault/secrets/env; npx nest start --debug 0.0.0.0:9050 --watch" ]
