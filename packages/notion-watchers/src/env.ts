import { Module } from '@nestjs/common';

export const GMAIL_CLIENT_ID = 'GMAIL_CLIENT_ID';

export const GMAIL_CLIENT_SECRET = 'GMAIL_CLIENT_SECRET';

export const GMAIL_REDIRECT_URL = 'GMAIL_REDIRECT_URL';

export const GMAIL_REFRESH_TOKEN = 'GMAIL_REFRESH_TOKEN';

export const NOTION_DATABASE_ID = 'NOTION_DATABASE_ID';

export const NOTION_APP_ID = 'NOTION_APP_ID';

export const NOTION_TIMEBLOCK_DATABASE_ID = 'NOTION_TIMEBLOCK_DATABASE_ID';

export const NOTION_TRANSACTIONS_DATABASE_ID =
  'NOTION_TRANSACTIONS_DATABASE_ID';

export const NOTION_TRANSACTION_CATEGORY_DATABASE_ID =
  'NOTION_TRANSACTION_CATEGORY_DATABASE_ID';

export const NOTION_ACCOUNTS_DATABASE_ID = 'NOTION_ACCOUNTS_DATABASE_ID';

export const NOTION_SETTINGS_PAGE_ID = 'NOTION_SETTINGS_PAGE_ID';

export const SENTRY_DSN_URL = 'SENTRY_DSN_URL';

const providers = [
  {
    provide: GMAIL_CLIENT_ID,
    useValue: process.env[GMAIL_CLIENT_ID] || '',
  },
  {
    provide: GMAIL_CLIENT_SECRET,
    useValue: process.env[GMAIL_CLIENT_SECRET] || '',
  },
  {
    provide: GMAIL_REDIRECT_URL,
    useValue: process.env[GMAIL_REDIRECT_URL] || '',
  },
  {
    provide: GMAIL_REFRESH_TOKEN,
    useValue: process.env[GMAIL_REFRESH_TOKEN] || '',
  },
  {
    provide: NOTION_DATABASE_ID,
    useValue: process.env[NOTION_DATABASE_ID] || '',
  },
  {
    provide: NOTION_APP_ID,
    useValue: process.env[NOTION_APP_ID] || '',
  },
  {
    provide: NOTION_TIMEBLOCK_DATABASE_ID,
    useValue: process.env[NOTION_TIMEBLOCK_DATABASE_ID] || '',
  },
  {
    provide: NOTION_TIMEBLOCK_DATABASE_ID,
    useValue: process.env[NOTION_TIMEBLOCK_DATABASE_ID] || '',
  },
  {
    provide: NOTION_TRANSACTIONS_DATABASE_ID,
    useValue: process.env[NOTION_TRANSACTIONS_DATABASE_ID] || '',
  },
  {
    provide: NOTION_TRANSACTION_CATEGORY_DATABASE_ID,
    useValue: process.env[NOTION_TRANSACTION_CATEGORY_DATABASE_ID] || '',
  },
  {
    provide: NOTION_ACCOUNTS_DATABASE_ID,
    useValue: process.env[NOTION_ACCOUNTS_DATABASE_ID] || '',
  },
  {
    provide: NOTION_SETTINGS_PAGE_ID,
    useValue: process.env[NOTION_SETTINGS_PAGE_ID] || '',
  },
  {
    provide: SENTRY_DSN_URL,
    useValue: process.env[SENTRY_DSN_URL] || '',
  },
];

@Module({
  providers,
  exports: providers.map(({ provide }) => provide),
})
export class AppEnv {}
