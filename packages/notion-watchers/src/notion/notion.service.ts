import * as moment from 'moment';
import { get } from 'lodash';
import { Client } from '@notionhq/client';
import { Inject, Injectable, Logger } from '@nestjs/common';
import {
  QueryDatabaseParameters,
  QueryDatabaseResponse,
} from '@notionhq/client/build/src/api-endpoints';
import {
  NOTION_ACCOUNTS_DATABASE_ID,
  NOTION_APP_ID,
  NOTION_DATABASE_ID,
  NOTION_SETTINGS_PAGE_ID,
  NOTION_TIMEBLOCK_DATABASE_ID,
  NOTION_TRANSACTIONS_DATABASE_ID,
  NOTION_TRANSACTION_CATEGORY_DATABASE_ID,
} from '../env';
import { Transaction } from 'src/types';

export enum NotionTasksTableKeys {
  /** `Name` _(string)_ - Task name */
  Name = 'Name',
  /** `Done` _(boolean)_ - Flags when a particular task is done and good to be scheduled again if applicable */
  Done = 'Done',
  /** `# of Done` _(number)_ - Count the times this has been flagged as Done */
  nOfDone = '# of Done',
  /** `Last Done By` _(person)_ - The last person that flagged this as Done */
  lastDoneBy = 'Last Done By',
  /** `Snooze` _(boolean)_ - Flags when a particular task is snoozed and should be shceduled again in the next time block */
  Snooze = 'Snooze',
  /** `# of Snooze` _(number)_ - Count the times this has been flagged as Snooze */
  nOfSnooze = '# of Snooze',
  /** `Last Snooze By` _(person)_ - The last person that flagged this as Snooze */
  lastSnoozeBy = 'Last Snooze By',
  /** `Updated By` _(person)_ - The last person that updated this task */
  updatedBy = 'Updated By',
  /** `Skip` _(boolean)_ - Flags when a particular task is Skiped should be scheduled again in the next iteration */
  Skip = 'Skip',
  /** `# of Skip` _(number)_ - Count the times this has been flagged as Skip */
  nOfSkip = '# of Skip',
  /** `Last Skip By` _(person)_ - The last person that flagged this as Skip */
  lastSkipBy = 'Last Skip By',
  /** `Start Date` _(Date)_ - Datetime when this task should start */
  StartDate = 'Start Date',
  /** `R. Cycles` _[(number)]_ - How many times this task should be schedule. Empty means no limit. 1 means only itself, no more tasks. Every re-schedule, substracts 1 form this property. */
  RecurrenceCycles = 'R. Cycles',
  /** `R. Unit` _[(days|weeks|months)]_ - Sets the unit to use when couting with `Recurrence`. */
  RecurrenceUnit = 'R. Unit',
  /** `R. Days` _[(string[])]_ - Sets the recurence to work on fixed week days. Value should be a multi select with values starting with a number from 1 to 7. I.e: `1|Monday`, `2 Tuesday` */
  RecurrenceDays = 'R. Days',
  /** `Recurrence` _[(number)]_ - Sets the recurrence to work by couting the amount of `R. Unit`. */
  Recurrence = 'Recurrence',
  /** `Time Block` _[relation]_ - The list of relations with time blocks */
  TimeBlock = 'Time Block',
}

export enum NotionTransactionsTableKeys {
  /** `Index` _(number)_ - Transaction index for the day */
  Index = 'Index',
  /** `Name` _(string)_ - Transaction name */
  Name = 'Name',
  /** `Debit` _(number)_ - Transaction debit */
  Debit = 'Debit',
  /** `Credit` _(number)_ - Transaction credit */
  Credit = 'Credit',
  /** `Currency` _(string)_ - Transaction currency */
  Currency = 'Currency',
  /** `Date` _(date)_ - Transaction date */
  Date = 'Date',
  /** `Detail` _(string)_ - Transaction detail */
  Detail = 'Detail',
  /** `Message` _(string)_ - Transaction message */
  Message = 'Message',
  /** `Destiny` _(string)_ - Transaction destiny */
  Destiny = 'Destiny',
  /** `Destiny Currency` _(string)_ - Transaction destiny currency */
  DestinyCurrency = 'Destiny Currency',
  /** `Destiny Amount` _(number)_ - Transaction destiny amount */
  DestinyAmount = 'Destiny Amount',
  /** `Balance` _(number)_ - Transaction source account balance */
  Balance = 'Balance',
  /** `Ex. Rate` _(number)_ - Transaction exchange rate */
  ExRate = 'Ex. Rate',
  /** `Account` _(string)_ - Transaction source account for founds */
  Account = 'Account',
  /** `Reference` _(string)_ - Transaction reference id */
  Reference = 'Reference',
  /** `Type` _(string)_ - Transaction type */
  Type = 'Type',
  /** `Category` _(string)_ - Transaction category */
  Category = 'Category',
}

export enum NotionCategoryTableKeys {
  /** `Name` _(string)_ - Transaction name */
  Name = 'Name',
  /** `Category` _(string)_ - Transaction category */
  Category = 'Category',
  /** `Pattern` _(string)_ - Transaction patter to look for transactions automatically */
  Pattern = 'Pattern',
  /** `R. Unit` _[(days|weeks|months)]_ - Sets the unit to use when couting with `Recurrence`. */
  RecurrenceUnit = 'R. Unit',
  /** `Recurrence` _[(number)]_ - Sets the recurrence to work by couting the amount of `R. Unit`. */
  Recurrence = 'Recurrence',
  /** `Payment Date` _(Date)_ - Datetime when this task should get paid */
  PaymentDate = 'Payment Date',
  /** `Limit Date` _(Date)_ - Datetime for the limit of when this task should get paid */
  LimitDate = 'Limit Date',
  /** `Paid` _(boolean)_ - The status of this payment task */
  Paid = 'Paid',
  /** `Scheduled` _(boolean)_ - The status of this payment task */
  Scheduled = 'Scheduled',
  /** `Active` _(boolean)_ - The status of this payment task */
  Active = 'Active',
  /** `Clone` _(boolean)_ - Clone this task */
  Clone = 'Clone',
  /** `Prev` _(relation)_ - Previous task */
  Prev = 'Prev',
  /** `Next` _(relation)_ - Next task */
  Next = 'Next',
  /** `Transactions` _(relation)_ - Transactions related to this category instance */
  Transactions = 'Transactions',
  /** `Priority` _(number)_ - Priority for category finder */
  Priority = 'Priority',
}

export enum NotionAccountsTableKeys {
  /** `Name` _(string)_ - Account name */
  Name = 'Name',
  /** `Cuenta Cliente` _(string)_ - Client account */
  CC = 'Cuenta Cliente',
  /** `Card` _(string)_ - Card */
  Card = 'Card',
}

export enum NotionSettingsTableKeys {
  /** `BAC Last Query Date` _(date)_ - BAC last query date to start new queries */
  BacLastQueryDate = 'BAC Last Query Date',
  /** `BAC Query Days` _(number)_ - BAC Query Days number */
  BacQueryDays = 'BAC Query Days',
}

export interface NotionSettings {
  bacLastQueryDate: Date;
  bacQueryDays: number;
}

@Injectable()
export class NotionService {
  private static logger = new Logger(NotionService.name);

  public static notion = new Client({
    auth: process.env.NOTION_API_KEY,
  });

  public extractProperties(page: any) {
    return Object.keys(page.properties)
      .map((key) => ({ key, value: page.properties[key] }))
      .reduce((result, prop) => {
        if (prop.value.type in this.typeCasters) {
          result[prop.key] = this.typeCasters[prop.value.type](prop.value);
        } else {
          NotionService.logger.log(
            'Couldnt process prop',
            prop.key,
            'with',
            prop.value,
          );
        }

        return result;
      }, {} as any);
  }

  private typeCasters = {
    number: (prop: any) => ({ number: get(prop, 'number', null) }),
    people: (prop: any) => ({
      people: get(prop, 'people', [])
        .map((p) => ({ id: p.id }))
        .filter((p) => p.id !== this.notionAppId),
    }),
    select: (prop: any) => ({
      select: prop?.select?.name ? { name: prop.select.name } : null,
    }),
    checkbox: (prop: any) => ({ checkbox: get(prop, 'checkbox', false) }),
    relation: (prop: any) => ({
      relation: get(prop, 'relation', []).map((r) => ({
        id: r.id,
      })),
    }),
    title: (prop: any) => ({
      title: get(prop, 'title', []).map((t) => ({
        text: { content: t.text.content },
      })),
    }),
    rich_text: (prop: any) => ({
      rich_text: get(prop, 'rich_text', null),
    }),
    multi_select: (prop: any) => ({
      multi_select: get(prop, 'multi_select', []).map((ms) => ({
        name: ms.name,
      })),
    }),
    date: (prop: any) => ({
      date: prop?.data?.start
        ? {
            start: get(prop, 'date.start', null),
            end: get(prop, 'date.end', null),
          }
        : null,
    }),
  };

  constructor(
    @Inject(NOTION_DATABASE_ID) private databaseId: string,
    @Inject(NOTION_APP_ID) private notionAppId: string,
    @Inject(NOTION_TIMEBLOCK_DATABASE_ID) private timeBlockDatabaseId: string,
    @Inject(NOTION_TRANSACTIONS_DATABASE_ID)
    private transactionsDatabaseId: string,
    @Inject(NOTION_TRANSACTION_CATEGORY_DATABASE_ID)
    private transactionCategoryDatabaseId: string,
    @Inject(NOTION_ACCOUNTS_DATABASE_ID)
    private accountsDatabaseId: string,
    @Inject(NOTION_SETTINGS_PAGE_ID)
    private settingsPageId: string,
  ) {}

  /**
   * Queries a table and apply recurrence tasks to it
   */
  async fetchRecurrentTasks(databaseId?: string) {
    const data = await NotionService.notion.databases.query({
      database_id: databaseId || this.databaseId,
      filter: {
        and: [
          {
            or: [
              // Is done
              {
                property: NotionTasksTableKeys.Done,
                checkbox: {
                  equals: true,
                },
              },
              // Is skip
              {
                property: NotionTasksTableKeys.Skip,
                checkbox: {
                  equals: true,
                },
              },
              // Is snooze
              {
                property: NotionTasksTableKeys.Snooze,
                checkbox: {
                  equals: true,
                },
              },
            ],
          },
          // Has a start date
          {
            property: NotionTasksTableKeys.StartDate,
            date: {
              is_not_empty: true as true,
            },
          },
          {
            or: [
              // Doesn't have a limit on recurrence
              {
                property: NotionTasksTableKeys.RecurrenceCycles,
                number: {
                  is_empty: true,
                },
              },
              // Or has a number of cycles higher than 1, the next task will have -1 cycles left
              {
                property: NotionTasksTableKeys.RecurrenceCycles,
                number: {
                  greater_than: 1,
                },
              },
            ],
          },
          {
            or: [
              {
                property: NotionTasksTableKeys.Recurrence,
                number: {
                  greater_than: 0,
                },
              },
              {
                property: NotionTasksTableKeys.RecurrenceDays,
                multi_select: {
                  is_not_empty: true,
                },
              },
            ],
          },
        ],
      },
    });

    return data.results;
  }

  /**
   * Queries a table and apply recurrence tasks to it duplicating task
   */
  async fetchRecurrentFinanceTasks(databaseId?: string) {
    const data = await NotionService.notion.databases.query({
      database_id: databaseId || this.transactionCategoryDatabaseId,
      filter: {
        or: [
          {
            and: [
              // Is paid
              {
                property: NotionCategoryTableKeys.Paid,
                checkbox: {
                  equals: true,
                },
              },
              // Is not scheduled yet
              {
                property: NotionCategoryTableKeys.Scheduled,
                checkbox: {
                  equals: false,
                },
              },
              // Payment date is older than today
              {
                property: NotionCategoryTableKeys.Active,
                checkbox: {
                  equals: false,
                },
              },
            ],
          },
          {
            and: [
              // Needs a clone
              {
                property: NotionCategoryTableKeys.Clone,
                checkbox: {
                  equals: true,
                },
              },
              // Has a start date
              {
                property: NotionCategoryTableKeys.PaymentDate,
                date: {
                  is_not_empty: true,
                },
              },
              // Has recurrence
              {
                property: NotionCategoryTableKeys.Recurrence,
                number: {
                  is_not_empty: true,
                },
              },
              // Has recurrence unit
              {
                property: NotionCategoryTableKeys.RecurrenceUnit,
                select: {
                  is_not_empty: true,
                },
              },
            ],
          },
        ],
      },
    });

    return data.results;
  }

  async createPage(
    page: Partial<QueryDatabaseResponse['results'][number]>,
    database_id = this.databaseId,
  ) {
    const response = await NotionService.notion.pages.create({
      parent: { database_id },
      ...page,
    } as any);

    return response;
  }

  async clonePage(
    page: QueryDatabaseResponse['results'][number],
    newStartDate: Date,
  ) {
    const originalProperties = this.extractProperties(page);
    const response = await this.createPage({
      properties: {
        ...originalProperties,
        [NotionTasksTableKeys.Done]: {
          checkbox: false,
        },
        [NotionTasksTableKeys.StartDate]: {
          date: {
            start: newStartDate.toISOString(),
            time_zone: 'America/Costa_Rica',
          },
        },
      },
    } as any);

    return response;
  }

  async processRecurrentTasks(results: QueryDatabaseResponse['results']) {
    for (let result of results) {
      const startDate = get(result, [
        'properties',
        NotionTasksTableKeys.StartDate,
        'date',
        'start',
      ]) as string;

      let updatedBy =
        get(result, [
          'properties',
          NotionTasksTableKeys.updatedBy,
          'last_edited_by',
          'type',
        ]) !== 'bot' &&
        (get(result, [
          'properties',
          NotionTasksTableKeys.updatedBy,
          'last_edited_by',
          'id',
        ]) as string);

      const lastSnoozeBy = get(result, [
        'properties',
        NotionTasksTableKeys.lastSnoozeBy,
        'people',
        'id',
      ]);

      const lastSkippedBy = get(result, [
        'properties',
        NotionTasksTableKeys.lastSnoozeBy,
        'people',
        'id',
      ]);

      const lastDonedBy = get(result, [
        'properties',
        NotionTasksTableKeys.lastSnoozeBy,
        'people',
        'id',
      ]);

      const done = get(result, [
        'properties',
        NotionTasksTableKeys.Done,
        'checkbox',
      ]) as boolean;

      const skip = get(result, [
        'properties',
        NotionTasksTableKeys.Skip,
        'checkbox',
      ]) as boolean;

      const snooze = get(result, [
        'properties',
        NotionTasksTableKeys.Snooze,
        'checkbox',
      ]) as boolean;

      let action = done
        ? 'done'
        : snooze
        ? 'snooze'
        : skip
        ? 'skip'
        : 'no-action';

      if (action === 'no-action') {
        NotionService.logger.log(`Skipping ${result.id} unknown action`);
        continue;
      }

      const nOfDone = get(result, [
        'properties',
        NotionTasksTableKeys.nOfDone,
        'number',
      ]) as number | void;

      const nOfSkip = get(result, [
        'properties',
        NotionTasksTableKeys.nOfSkip,
        'number',
      ]) as number | void;

      const nOfSnooze = get(result, [
        'properties',
        NotionTasksTableKeys.nOfSnooze,
        'number',
      ]) as number | void;

      const value = get(result, [
        'properties',
        NotionTasksTableKeys.Recurrence,
        'number',
      ]) as number | void;

      const unit = get(result, [
        'properties',
        NotionTasksTableKeys.RecurrenceUnit,
        'select',
        'name',
      ]) as any;

      const days = get(result, [
        'properties',
        NotionTasksTableKeys.RecurrenceDays,
        'multi_select',
      ]).map(({ name }) => name) as string[];

      let cycles = get(result, [
        'properties',
        NotionTasksTableKeys.RecurrenceCycles,
        'number',
      ]) as number;

      if (cycles) {
        cycles--;
      }

      let newStartDate: Date;

      let initialStartDate = get(result, [
        'properties',
        NotionTasksTableKeys.StartDate,
        'date',
        'start',
      ]);

      const [match, operator, hours] = Array.from(
        initialStartDate.match(/(\+|-)(\d{2}):(\d{2})$/) || [],
      );
      if (match) {
        initialStartDate = moment(initialStartDate)[
          operator === '+' ? 'add' : 'subtract'
        ](hours, 'hours');
      }

      // Process by unit
      if (value && ['days', 'weeks', 'months'].includes(unit)) {
        newStartDate = await this.processByUnit(
          initialStartDate,
          value,
          unit,
          newStartDate,
        );
        NotionService.logger.log(
          `Getting next iteration by unit for ${result.id} ${newStartDate}`,
        );
      }

      // Process by day of week
      if (days.length) {
        newStartDate = await this.processByDaysOfWeek(
          newStartDate
            ? moment(newStartDate).subtract(7, 'days').toDate()
            : initialStartDate,
          days,
        );
        NotionService.logger.log(
          `Getting next iteration by day for ${result.id} ${newStartDate}`,
        );
      }

      if (!newStartDate) {
        NotionService.logger.log(`Skipping ${result.id} no new date possible`);
        continue;
      }

      if (action === 'snooze') {
        const timeBlocks = get(
          result,
          ['properties', NotionTasksTableKeys.TimeBlock, 'relation'],
          [],
        ) as any[];

        const schedules = await NotionService.notion.databases.query({
          database_id: this.timeBlockDatabaseId,
          filter: {
            or: timeBlocks.map((tb) => ({
              property: NotionTasksTableKeys.TimeBlock,
              relation: {
                contains: tb.id,
              },
            })),
          },
          sorts: [
            {
              property: NotionTasksTableKeys.StartDate,
              direction: 'ascending',
            },
          ],
        });

        if (schedules.results.length) {
          const nextScheduleStartDate = schedules.results
            .map(
              (schedule) =>
                new Date(
                  get(schedule, [
                    'properties',
                    NotionTasksTableKeys.StartDate,
                    'formula',
                    'date',
                    'start',
                  ]) as string,
                ),
            )
            .find((date) => date > new Date(startDate) && date > new Date());

          if (nextScheduleStartDate < newStartDate) {
            NotionService.logger.log(
              `New timeblock found for task ${result.id} ${nextScheduleStartDate}`,
            );
            newStartDate = nextScheduleStartDate;
          } else {
            NotionService.logger.log(
              `Overriding snooze action as skip for ${result.id}`,
            );
            action = 'skip';
          }
        } else {
          NotionService.logger.log(
            `Overriding snooze action as skip for ${result.id}`,
          );
          action = 'skip';
        }
      }

      NotionService.logger.log(`Processed ${result.id}`);
      await NotionService.notion.pages.update({
        page_id: result.id,
        properties: {
          [NotionTasksTableKeys.StartDate]: {
            date: {
              start: newStartDate.toISOString(),
              time_zone: 'America/Costa_Rica',
            },
          },
          [NotionTasksTableKeys.Done]: {
            checkbox: false,
          },
          [NotionTasksTableKeys.Skip]: {
            checkbox: false,
          },
          [NotionTasksTableKeys.Snooze]: {
            checkbox: false,
          },
          ...(action !== 'done'
            ? {}
            : {
                [NotionTasksTableKeys.nOfDone]: {
                  number: (nOfDone || 0) + 1,
                },
                [NotionTasksTableKeys.lastDoneBy]: {
                  people:
                    updatedBy || lastDonedBy
                      ? [{ id: updatedBy || lastDonedBy }]
                      : [],
                },
                [NotionTasksTableKeys.nOfSkip]: {
                  number: 0,
                },
                [NotionTasksTableKeys.nOfSnooze]: {
                  number: 0,
                },
              }),
          ...(action !== 'skip'
            ? {}
            : {
                [NotionTasksTableKeys.nOfSkip]: {
                  number: (nOfSkip || 0) + 1,
                },
                [NotionTasksTableKeys.lastSkipBy]: {
                  people:
                    updatedBy || lastSkippedBy
                      ? [{ id: updatedBy || lastSkippedBy }]
                      : [],
                },
              }),
          ...(action !== 'snooze'
            ? {}
            : {
                [NotionTasksTableKeys.nOfSnooze]: {
                  number: (nOfSnooze || 0) + 1,
                },
                [NotionTasksTableKeys.lastSnoozeBy]: {
                  people:
                    updatedBy || lastSnoozeBy
                      ? [{ id: updatedBy || lastSnoozeBy }]
                      : [],
                },
              }),
          ...(cycles
            ? {
                [NotionTasksTableKeys.RecurrenceCycles]: {
                  number: cycles,
                },
              }
            : {}),
        } as any,
      });
    }
  }

  async processRecurrentFinancialTasks(
    results: QueryDatabaseResponse['results'],
  ) {
    for (let result of results) {
      const name = get(result, [
        'properties',
        NotionCategoryTableKeys.Name,
        'title',
        0,
        'text',
        'content',
      ]) as string;

      const category = get(result, [
        'properties',
        NotionCategoryTableKeys.Category,
        'select',
        'name',
      ]) as string;

      const lastTaskResult = await NotionService.notion.databases.query({
        database_id: this.transactionCategoryDatabaseId,
        filter: {
          and: [
            {
              property: NotionCategoryTableKeys.Name,
              title: {
                equals: name,
              },
            },
            ...(category
              ? [
                  {
                    property: NotionCategoryTableKeys.Category,
                    select: {
                      equals: category,
                    },
                  },
                ]
              : []),
          ],
        },
        sorts: [
          {
            property: NotionCategoryTableKeys.PaymentDate,
            direction: 'descending',
          },
        ],
        page_size: 1,
      });

      const paymentDate = moment(
        (get(lastTaskResult.results, [
          0,
          'properties',
          NotionCategoryTableKeys.PaymentDate,
          'date',
          'start',
        ]) as string) || '',
      );

      if (!paymentDate.isValid()) {
        NotionService.logger.log(`Skipping ${result.id} no payment date`);
        continue;
      }

      const paid = get(result, [
        'properties',
        NotionCategoryTableKeys.Paid,
        'checkbox',
      ]) as boolean;

      const clone = get(result, [
        'properties',
        NotionCategoryTableKeys.Clone,
        'checkbox',
      ]) as boolean;

      let action = paid ? 'paid' : clone ? 'clone' : 'no-action';

      if (action === 'no-action') {
        NotionService.logger.log(`Skipping ${result.id} unknown action`);
        continue;
      }

      const value = get(result, [
        'properties',
        NotionCategoryTableKeys.Recurrence,
        'number',
      ]) as number | void;

      const unit = get(result, [
        'properties',
        NotionCategoryTableKeys.RecurrenceUnit,
        'select',
        'name',
      ]) as any;

      let newStartDate: Date;

      // Process by unit
      if (value && ['days', 'weeks', 'months'].includes(unit)) {
        newStartDate = await this.nextDateByUnit(
          paymentDate.toDate(),
          value,
          unit,
        );
        NotionService.logger.log(
          `Getting next iteration by unit for ${result.id} ${newStartDate}`,
        );
      }

      if (!newStartDate) {
        NotionService.logger.log(`Skipping ${result.id} no new date possible`);
        continue;
      }

      NotionService.logger.log(`Processed ${result.id}`);
      await NotionService.notion.pages.update({
        page_id: result.id,
        properties: {
          ...(action === 'paid' && {
            [NotionCategoryTableKeys.Scheduled]: {
              checkbox: true,
            },
          }),
          [NotionCategoryTableKeys.Clone]: {
            checkbox: false,
          },
        } as any,
      });

      const lastTask = lastTaskResult.results[0];

      const originalProperties = this.extractProperties(result);
      const newPage = await this.createPage(
        {
          properties: {
            ...originalProperties,
            [NotionCategoryTableKeys.PaymentDate]: {
              date: {
                start: moment(newStartDate).format('YYYY-MM-DD'),
              },
            },
            [NotionCategoryTableKeys.Paid]: {
              checkbox: false,
            },
            [NotionCategoryTableKeys.Clone]: {
              checkbox: false,
            },
            [NotionCategoryTableKeys.Prev]: {
              relation: [
                {
                  id: lastTask.id,
                },
              ],
            },
            [NotionCategoryTableKeys.Transactions]: {
              relation: [],
            },
          },
        },
        this.transactionCategoryDatabaseId,
      );

      await NotionService.notion.pages.update({
        page_id: lastTask.id,
        properties: {
          [NotionCategoryTableKeys.Next]: {
            relation: [
              {
                id: newPage.id,
              },
            ],
          },
        },
      });
    }
  }

  async getSettings(): Promise<NotionSettings> {
    if (!this.settingsPageId) {
      return {
        bacLastQueryDate: moment(moment().format('YYYY-MM-DD')).toDate(),
        bacQueryDays: 1,
      };
    }

    const settings = await NotionService.notion.pages.retrieve({
      page_id: this.settingsPageId,
    });

    return {
      bacLastQueryDate: moment(
        get(
          settings,
          [
            'properties',
            NotionSettingsTableKeys.BacLastQueryDate,
            'date',
            'start',
          ],
          moment().format('YYYY-MM-DD'),
        ),
      ).toDate(),
      bacQueryDays: parseInt(
        get(
          settings,
          ['properties', NotionSettingsTableKeys.BacQueryDays, 'number'],
          '1',
        ),
        10,
      ),
    };
  }

  async setSettings(settings: Partial<NotionSettings>): Promise<void> {
    if (!this.settingsPageId) {
      return;
    }

    await NotionService.notion.pages.update({
      page_id: this.settingsPageId,
      properties: {
        ...(settings.bacQueryDays
          ? {
              [NotionSettingsTableKeys.BacQueryDays]: {
                number: settings.bacQueryDays,
              },
            }
          : {}),
        ...(settings.bacLastQueryDate
          ? {
              [NotionSettingsTableKeys.BacLastQueryDate]: {
                date: {
                  start: moment(settings.bacLastQueryDate).format('YYYY-MM-DD'),
                },
              },
            }
          : {}),
      },
    });
  }

  async processTransactions(transactions: Transaction[]) {
    const categories = [];
    const categoriesQuery: QueryDatabaseParameters = {
      database_id: this.transactionCategoryDatabaseId,
      filter: {
        and: [
          {
            property: NotionCategoryTableKeys.Pattern,
            rich_text: {
              is_not_empty: true,
            },
          },
          {
            property: NotionCategoryTableKeys.Scheduled,
            checkbox: {
              equals: false,
            },
          },
          {
            property: NotionCategoryTableKeys.PaymentDate,
            date: {
              is_not_empty: true,
            },
          },
        ],
      },
      sorts: [
        {
          property: NotionCategoryTableKeys.Priority,
          direction: 'descending',
        },
      ],
    };
    let categoriesResult = await NotionService.notion.databases.query(
      categoriesQuery,
    );

    while (categoriesResult.has_more) {
      categoriesResult = await NotionService.notion.databases.query({
        ...(categoriesQuery as any),
        start_cursor: categoriesResult.next_cursor,
      });
      categories.push(...categoriesResult.results);
    }

    const accounts = await NotionService.notion.databases.query({
      database_id: this.accountsDatabaseId,
      filter: {
        and: [
          {
            property: NotionAccountsTableKeys.CC,
            number: {
              is_not_empty: true,
            },
          },
        ],
      },
    });

    const results = [];

    for (const transaction of transactions) {
      const account = accounts.results.find((account) => {
        const cc = get(account, [
          'properties',
          NotionAccountsTableKeys.CC,
          'number',
        ]);
        const card = get(account, [
          'properties',
          NotionAccountsTableKeys.Card,
          'number',
        ]);

        return cc === transaction.account || card === transaction.account;
      });

      const pageResult =
        transaction.ref && transaction.detail
          ? await NotionService.notion.databases.query({
              database_id: this.transactionsDatabaseId,
              filter: {
                and: [
                  ...(account
                    ? [
                        {
                          property: NotionTransactionsTableKeys.Account,
                          relation: {
                            contains: account.id,
                          },
                        },
                      ]
                    : []),
                  ...(transaction.ref
                    ? [
                        {
                          property: NotionTransactionsTableKeys.Reference,
                          number: {
                            equals: transaction.ref,
                          },
                        },
                      ]
                    : []),
                  ...(transaction.detail
                    ? [
                        {
                          property: NotionTransactionsTableKeys.Detail,
                          rich_text: {
                            equals: transaction.detail,
                          },
                        },
                      ]
                    : []),
                  ...(transaction.balance
                    ? [
                        {
                          property: NotionTransactionsTableKeys.Balance,
                          number: {
                            equals: transaction.balance,
                          },
                        },
                      ]
                    : []),
                ],
              },
            })
          : { results: [] };

      if (pageResult.results.length) {
        NotionService.logger.log(
          `Skipping transaction ${transaction.ref} transaction already created`,
        );
        continue;
      }

      const category = categories.find((category) => {
        const matchTransactions = get(category, [
          'properties',
          NotionCategoryTableKeys.Pattern,
          'rich_text',
          0,
          'text',
          'content',
        ]);

        const recurrenceUnit = get(category, [
          'properties',
          NotionCategoryTableKeys.RecurrenceUnit,
          'select',
          'name',
        ]);

        const recurrence = get(category, [
          'properties',
          NotionCategoryTableKeys.Recurrence,
          'number',
        ]);

        const paymentDate = new Date(
          get(category, [
            'properties',
            NotionCategoryTableKeys.PaymentDate,
            'date',
            'start',
          ]) as string,
        );

        const matchDate = moment(transaction.date).isBetween(
          paymentDate,
          moment(paymentDate).add(recurrence, recurrenceUnit),
        );

        const matchTransactionsRegex = new RegExp(matchTransactions, 'i');
        const matchPattern =
          matchTransactionsRegex.test(transaction.detail) ||
          matchTransactionsRegex.test(transaction.message);

        return matchPattern && matchDate;
      });

      results.push(
        await this.createPage(
          {
            properties: {
              [NotionTransactionsTableKeys.Index]: {
                type: 'number',
                number: transaction.index || null,
              } as any,
              [NotionTransactionsTableKeys.Name]: {
                type: 'title',
                title: [
                  {
                    text: {
                      content: `${transaction.detail || ''} - ${
                        transaction.ref || ''
                      }`,
                    },
                  },
                ],
              } as any,
              [NotionTransactionsTableKeys.Debit]: {
                type: 'number',
                number: transaction.debit || null,
              } as any,
              [NotionTransactionsTableKeys.Credit]: {
                type: 'number',
                number: transaction.credit || null,
              } as any,
              [NotionTransactionsTableKeys.Detail]: {
                type: 'rich_text',
                rich_text: [{ text: { content: transaction.detail || '' } }],
              } as any,
              [NotionTransactionsTableKeys.Currency]: {
                type: 'select',
                select: { name: transaction.currency },
              } as any,
              [NotionTransactionsTableKeys.Date]: {
                type: 'date',
                date: {
                  start: moment(transaction.date)
                    .subtract(6, 'hours')
                    .toISOString(),
                  time_zone: 'America/Costa_Rica',
                },
              } as any,
              [NotionTransactionsTableKeys.Account]: {
                type: 'relation',
                relation: account ? [{ id: account.id || '' }] : [],
              } as any,
              [NotionTransactionsTableKeys.Reference]: {
                type: 'number',
                number: transaction.ref || null,
              } as any,
              [NotionTransactionsTableKeys.ExRate]: {
                type: 'number',
                number: transaction.exchangeRate || null,
              } as any,
              [NotionTransactionsTableKeys.Balance]: {
                type: 'number',
                number: transaction.balance || null,
              } as any,
              [NotionTransactionsTableKeys.Destiny]: {
                type: 'rich_text',
                rich_text: [{ text: { content: transaction.destiny || '' } }],
              } as any,
              [NotionTransactionsTableKeys.DestinyAmount]: {
                type: 'number',
                number: transaction.destinyAmount || null,
              } as any,
              [NotionTransactionsTableKeys.DestinyCurrency]: {
                type: 'select',
                select: transaction.destinyCurrency
                  ? { name: transaction.destinyCurrency }
                  : null,
              } as any,
              [NotionTransactionsTableKeys.Message]: {
                type: 'rich_text',
                rich_text: [{ text: { content: transaction.message || '' } }],
              } as any,
              ...(transaction.type
                ? {
                    [NotionTransactionsTableKeys.Type]: {
                      type: 'select',
                      select: { name: transaction.type },
                    } as any,
                  }
                : {}),
              ...(category
                ? {
                    [NotionTransactionsTableKeys.Category]: {
                      type: 'relation',
                      relation: [
                        {
                          id: category.id,
                        },
                      ],
                    } as any,
                  }
                : {}),
            },
          },
          this.transactionsDatabaseId,
        ),
      );
    }

    return results;
  }

  getStartDate(startDate: Date, newDateFn: (startDate: Date) => Date) {
    const newStartDate = newDateFn(startDate);

    if (newStartDate >= moment().startOf('day').toDate()) {
      return newStartDate;
    }

    return this.getStartDate(newStartDate, newDateFn);
  }

  async nextDateByUnit(
    startDate: Date,
    value: number,
    unit: 'days' | 'weeks' | 'months',
  ) {
    return moment(startDate).add(value, unit).toDate();
  }

  async processByUnit(
    initialStartDate: Date,
    value: number,
    unit: 'days' | 'weeks' | 'months',
    newDate?: Date,
  ) {
    const startDateFn = (startDate: Date) =>
      moment(startDate).add(value, unit).toDate();
    const start = this.getStartDate(initialStartDate, startDateFn);

    return newDate && start > newDate ? newDate : start;
  }

  async processByDaysOfWeek(
    initialStartDate: Date,
    days: string[],
    // action: 'done'|'skip'|'snooze'
  ) {
    const parsedDays = days.map((str) => parseInt(str[0]));

    // Calculate next day by weekday list
    const startDateFn = (startDate: Date) => {
      const nextDayOfWeek =
        parsedDays.find((n) => n > moment(startDate).day()) || parsedDays[0];

      return moment(startDate)
        .add(7, 'days')
        .subtract(
          moment(startDate)
            .add(7, 'days')
            .subtract(nextDayOfWeek, 'days')
            .day(),
          'days',
        )
        .toDate();
    };

    const start = this.getStartDate(initialStartDate, startDateFn);

    return start;
  }
}
