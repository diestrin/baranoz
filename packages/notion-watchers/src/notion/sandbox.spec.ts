import { Test, TestingModule } from '@nestjs/testing';
import { get } from 'lodash';
import * as moment from 'moment';

import {
  NotionCategoryTableKeys,
  NotionService,
  NotionTransactionsTableKeys,
} from './notion.service';
import {
  AppEnv,
  NOTION_DATABASE_ID,
  NOTION_TRANSACTION_CATEGORY_DATABASE_ID,
  NOTION_TRANSACTIONS_DATABASE_ID,
} from '../env';

jest.setTimeout(1000 * 60 * 5);

describe.skip('Sandbox', () => {
  const database_id = process.env.NOTION_DATABASE_ID;

  let service: NotionService;
  let databaseId = '';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppEnv],
      providers: [NotionService],
    }).compile();

    service = module.get<NotionService>(NotionService);
    databaseId = module.get<string>(NOTION_DATABASE_ID);
  });

  it('should split a task with multiple zones', async () => {
    const newPageResult = await NotionService.notion.databases.query({
      database_id: database_id,
      filter: {
        property: 'Zone Count',
        number: {
          greater_than: 1,
        },
      },
      page_size: 50,
    });

    for (let page of newPageResult.results) {
      const zones: any[] = get(page, 'properties.Zone.multi_select');
      const originalProperties = service.extractProperties(page);

      const results = await Promise.all(
        zones.slice(1).map((zone) => {
          console.log('Cloning', page.id, 'with zone', zone.name);

          return NotionService.notion.pages.create({
            parent: { database_id: databaseId },
            properties: {
              ...originalProperties,
              Zone: {
                multi_select: [
                  {
                    name: zone.name,
                  },
                ],
              },
            },
          } as any);
        }),
      );

      console.log('Updating', page.id, 'with zone', zones[0].name);
      await NotionService.notion.pages.update({
        page_id: page.id,
        properties: {
          Zone: {
            multi_select: [
              {
                name: zones[0].name,
              },
            ],
          },
        },
      });

      console.log(
        results.length,
        'pages created',
        results.map((r) => r.id),
      );
    }
  });
});

describe.skip('Transaction sandbox', () => {
  let service: NotionService;
  let databaseId = '';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppEnv],
      providers: [NotionService],
    }).compile();

    service = module.get<NotionService>(NotionService);
    databaseId = module.get<string>(NOTION_TRANSACTION_CATEGORY_DATABASE_ID);
  });

  it('should apply task recurrence', async () => {
    const categories = await NotionService.notion.databases.query({
      database_id: databaseId,
      filter: {
        and: [
          {
            property: NotionCategoryTableKeys.Paid,
            checkbox: {
              equals: false,
            },
          },
          {
            property: NotionCategoryTableKeys.PaymentDate,
            date: {
              is_not_empty: true,
            },
          },
        ],
      },
      page_size: 50,
    });

    console.log(categories.results.length, 'pages found');

    for (let page of categories.results) {
      try {
        const startDate = moment(
          get(
            page,
            [
              'properties',
              NotionCategoryTableKeys.PaymentDate,
              'date',
              'start',
            ],
            '',
          ),
        );

        const endDate = moment(
          get(
            page,
            ['properties', NotionCategoryTableKeys.PaymentDate, 'date', 'end'],
            '',
          ),
        );

        const originalProperties = service.extractProperties(page);

        const recurrenceUnit = get(
          page,
          [
            'properties',
            NotionCategoryTableKeys.RecurrenceUnit,
            'select',
            'name',
          ],
          '',
        );
        const recurrenceValue = get(
          page,
          ['properties', NotionCategoryTableKeys.Recurrence, 'number'],
          '',
        );

        const cycles =
          Math.floor(
            moment().diff(startDate, recurrenceUnit) / recurrenceValue,
          ) + 1;

        const dates = Array(cycles)
          .fill(0)
          .map((_, i) => ({
            start: startDate
              .clone()
              .add((i + 1) * recurrenceValue, recurrenceUnit),
            ...(endDate.isValid()
              ? {
                  end: endDate
                    .clone()
                    .add((i + 1) * recurrenceValue, recurrenceUnit),
                }
              : {}),
            time_zone: 'America/Costa_Rica',
          }));

        const results = await Promise.all(
          dates.map((date) => {
            console.log('Cloning', page.id, 'with date', date.start);

            return NotionService.notion.pages.create({
              parent: { database_id: databaseId },
              properties: {
                ...originalProperties,
                [NotionCategoryTableKeys.PaymentDate]: { date },
              },
            } as any);
          }),
        );

        // console.log('Updating', page.id, 'with zone', zones[0].name);
        // await NotionService.notion.pages.update({
        //   page_id: page.id,
        //   properties: {
        //     Zone: {
        //       multi_select: [
        //         {
        //           name: zones[0].name,
        //         },
        //       ],
        //     },
        //   },
        // });

        console.log(
          results.length,
          'pages created',
          results.map((r) => r.id),
        );
      } catch (e) {
        console.log('Failed to process', page.id, e);
      }
    }
  });
});

describe('Transaction tasks categories', () => {
  let service: NotionService;
  let categoryDatabaseId = '';
  let transactionsDatabaseId = '';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppEnv],
      providers: [NotionService],
    }).compile();

    service = module.get<NotionService>(NotionService);
    categoryDatabaseId = module.get<string>(
      NOTION_TRANSACTION_CATEGORY_DATABASE_ID,
    );
    transactionsDatabaseId = module.get<string>(
      NOTION_TRANSACTIONS_DATABASE_ID,
    );
  });

  it('should apply task category', async () => {
    const categories = [];
    const categoriesQuery = {
      database_id: categoryDatabaseId,
      filter: {
        and: [
          {
            property: NotionCategoryTableKeys.Pattern,
            rich_text: {
              is_not_empty: true,
            },
          },
          {
            property: NotionCategoryTableKeys.Paid,
            checkbox: {
              equals: false,
            },
          },
          {
            property: NotionCategoryTableKeys.PaymentDate,
            date: {
              is_not_empty: true,
            },
          },
        ],
      },
      sorts: [
        {
          property: NotionCategoryTableKeys.Priority,
          direction: 'descending',
        },
        {
          property: NotionCategoryTableKeys.PaymentDate,
          direction: 'descending',
        },
      ],
    };
    let categoriesResult = await NotionService.notion.databases.query(
      categoriesQuery as any,
    );

    while (categoriesResult.has_more) {
      categoriesResult = await NotionService.notion.databases.query({
        ...(categoriesQuery as any),
        start_cursor: categoriesResult.next_cursor,
      });
      categories.push(...categoriesResult.results);
    }

    console.log(categories.length, 'categories found');

    const transactions = await NotionService.notion.databases.query({
      database_id: transactionsDatabaseId,
      filter: {
        and: [
          {
            property: NotionTransactionsTableKeys.Date,
            date: {
              on_or_before: '2022-07-01',
            },
          },
          {
            property: NotionTransactionsTableKeys.Date,
            date: {
              on_or_after: '2022-06-30',
            },
          },
        ],
      },
    });

    console.log(transactions.results.length, 'transactions found');

    for (let transaction of transactions.results) {
      try {
        const tDate = get(transaction, [
          'properties',
          NotionTransactionsTableKeys.Date,
          'date',
          'start',
        ]);

        const tName = get(transaction, [
          'properties',
          NotionTransactionsTableKeys.Name,
          'title',
          0,
          'text',
          'content',
        ]);

        const tMessage = get(transaction, [
          'properties',
          NotionTransactionsTableKeys.Message,
          'rich_text',
          0,
          'text',
          'content',
        ]);

        const category = categories.find((cat) => {
          const matchTransactions = get(cat, [
            'properties',
            NotionCategoryTableKeys.Pattern,
            'rich_text',
          ]).reduce(
            (result, line) => result + get(line, ['text', 'content']),
            '',
          );

          // console.log('regex', matchTransactions);

          const recurrenceUnit = get(cat, [
            'properties',
            NotionCategoryTableKeys.RecurrenceUnit,
            'select',
            'name',
          ]);

          const recurrence = get(cat, [
            'properties',
            NotionCategoryTableKeys.Recurrence,
            'number',
          ]);

          const paymentDate = new Date(
            get(cat, [
              'properties',
              NotionCategoryTableKeys.PaymentDate,
              'date',
              'start',
            ]) as string,
          );

          const matchDate = moment(tDate).isBetween(
            paymentDate,
            moment(paymentDate).add(recurrence, recurrenceUnit),
          );

          const matchTransactionsRegex = new RegExp(matchTransactions, 'i');
          const matchPattern =
            matchTransactionsRegex.test(tName) ||
            matchTransactionsRegex.test(tMessage);
          // console.log('match', matchTransactions, tName, matchPattern);

          return matchPattern && matchDate;
        });

        if (!category) {
          console.log('No match for', tName, tMessage);
          // await NotionService.notion.pages.update({
          //   page_id: transaction.id,
          //   properties: {
          //     [NotionTransactionsTableKeys.Category]: {
          //       relation: [],
          //     },
          //   },
          // });
          continue;
        }

        console.log(
          'Updating',
          tName,
          'with category',
          get(category, [
            'properties',
            NotionCategoryTableKeys.Name,
            'title',
            0,
            'text',
            'content',
          ]),
        );

        await NotionService.notion.pages.update({
          page_id: transaction.id,
          properties: {
            [NotionTransactionsTableKeys.Category]: {
              relation: [
                {
                  id: category.id,
                },
              ],
            },
          },
        });
      } catch (e) {
        console.log('Failed to process', transaction.id, e);
      }
    }
  });
});
