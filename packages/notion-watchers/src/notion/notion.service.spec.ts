import * as moment from 'moment';
import { Test, TestingModule } from '@nestjs/testing';

import {
  NotionCategoryTableKeys,
  NotionService,
  NotionTasksTableKeys,
  NotionTransactionsTableKeys,
} from './notion.service';
import { get } from 'lodash';
import {
  AppEnv,
  NOTION_DATABASE_ID,
  NOTION_TRANSACTIONS_DATABASE_ID,
  NOTION_TRANSACTION_CATEGORY_DATABASE_ID,
} from '../env';

jest.setTimeout(1000 * 60 * 10);

describe('NotionService', () => {
  const clean = true;
  let database_id = '';
  let service: NotionService;
  let page_id = '';
  const createdPagesIds = [];

  describe('Tasks Database', () => {
    beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [AppEnv],
        providers: [NotionService],
      }).compile();

      service = module.get<NotionService>(NotionService);
      database_id = module.get<string>(NOTION_DATABASE_ID);

      const data = await service.clonePage(
        {
          parent: { database_id },
          properties: {
            [NotionTasksTableKeys.RecurrenceCycles]: {
              type: 'number',
              number: null,
            },
            [NotionTasksTableKeys.RecurrenceDays]: {
              type: 'multi_select',
              multi_select: [],
            },
            [NotionTasksTableKeys.RecurrenceUnit]: {
              type: 'select',
              select: null,
            },
            [NotionTasksTableKeys.Recurrence]: {
              type: 'number',
              number: 9,
            },
            [NotionTasksTableKeys.Name]: {
              type: 'title',
              title: [
                {
                  text: {
                    content: 'notion-watchers',
                  },
                },
              ],
            },
          },
        } as any,
        new Date(),
      );

      page_id = data.id;
    });

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [AppEnv],
        providers: [NotionService],
      }).compile();

      service = module.get<NotionService>(NotionService);

      // Reset task status
      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionTasksTableKeys.StartDate]: {
            date: {
              start: moment().subtract(6, 'hours').toISOString(),
              time_zone: 'America/Costa_Rica',
            },
          },
          [NotionTasksTableKeys.Done]: {
            checkbox: true,
          },
          [NotionTasksTableKeys.RecurrenceCycles]: {
            number: null,
          },
          [NotionTasksTableKeys.RecurrenceDays]: {
            multi_select: [],
          },
          [NotionTasksTableKeys.RecurrenceUnit]: {
            select: null,
          },
          [NotionTasksTableKeys.Recurrence]: {
            number: null,
          },
        },
      });
    });

    afterEach(async () => {
      const results = await NotionService.notion.databases.query({
        database_id: database_id,
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                starts_with: 'notion-watchers',
              },
            },
          ],
        },
      });

      for (let page of createdPagesIds) {
        await NotionService.notion.pages.update({
          page_id: page,
          archived: true,
        });
      }

      for (let result of results.results) {
        if (result.id === page_id) break;

        await NotionService.notion.pages.update({
          page_id: result.id,
          archived: true,
        });
      }
    });

    afterAll(async () => {
      await NotionService.notion.pages.update({
        page_id,
        archived: true,
      });
    });

    it('should query for settings', async () => {
      const settings = await service.getSettings();

      expect(settings).toBeDefined();
      expect(settings.bacQueryDays).toBeDefined();
      expect(settings.bacLastQueryDate).toBeDefined();
    });

    it('should handle daily recurrent tasks', async () => {
      expect.assertions(2);

      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionTasksTableKeys.RecurrenceUnit]: {
            select: {
              name: 'days',
            },
          },
          [NotionTasksTableKeys.Recurrence]: {
            number: 2,
          },
        },
      });

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      const newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).toISOString(),
      ).toEqual(
        moment(
          get(originalPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        )
          .add(
            get(originalPage, [
              'properties',
              NotionTasksTableKeys.Recurrence,
              'number',
            ]),
            get(originalPage, [
              'properties',
              NotionTasksTableKeys.RecurrenceUnit,
              'select',
              'name',
            ]),
          )
          .toISOString(),
      );
    });

    it('should handle weekly recurrent tasks', async () => {
      expect.assertions(2);

      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionTasksTableKeys.RecurrenceUnit]: {
            select: {
              name: 'weeks',
            },
          },
          [NotionTasksTableKeys.Recurrence]: {
            number: 2,
          },
        },
      });

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      const newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).toISOString(),
      ).toEqual(
        moment(
          get(originalPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        )
          .add(
            get(originalPage, [
              'properties',
              NotionTasksTableKeys.Recurrence,
              'number',
            ]),
            get(originalPage, [
              'properties',
              NotionTasksTableKeys.RecurrenceUnit,
              'select',
              'name',
            ]),
          )
          .toISOString(),
      );
    });

    it('should handle monthly recurrent tasks', async () => {
      expect.assertions(2);

      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionTasksTableKeys.RecurrenceUnit]: {
            select: {
              name: 'months',
            },
          },
          [NotionTasksTableKeys.Recurrence]: {
            number: 2,
          },
        },
      });

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      const newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).toISOString(),
      ).toEqual(
        moment(
          get(originalPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        )
          .add(
            get(originalPage, [
              'properties',
              NotionTasksTableKeys.Recurrence,
              'number',
            ]),
            get(originalPage, [
              'properties',
              NotionTasksTableKeys.RecurrenceUnit,
              'select',
              'name',
            ]),
          )
          .toISOString(),
      );
    });

    it('should handle weekday recurrent tasks', async () => {
      expect.assertions(6);

      const getNextWeekDay = (startDate: Date, nextDayOfWeek: number) => {
        return moment(startDate)
          .add(7, 'days')
          .subtract(
            moment(startDate)
              .add(7, 'days')
              .subtract(nextDayOfWeek, 'days')
              .day(),
            'days',
          );
      };

      const days = [
        { name: '1|Monday' },
        { name: '3|Wednesday' },
        { name: '5|Friday' },
      ];

      const parsedDays = days.map((str) => parseInt(str.name[0]));

      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionTasksTableKeys.RecurrenceDays]: {
            multi_select: days,
          },
        },
      });

      // First weekday

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      let newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      let newPage = newPageResult.results[0];

      let startDate = get(originalPage, [
        'properties',
        NotionTasksTableKeys.StartDate,
        'date',
        'start',
      ]);
      let nextDayOfWeek =
        parsedDays.find((n) => n > moment(startDate).day()) || parsedDays[0];
      let nextDate = getNextWeekDay(startDate, nextDayOfWeek);

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).format('YYYY-MM-DD'),
      ).toBe(nextDate.format('YYYY-MM-DD'));

      // Second weekday

      await NotionService.notion.pages.update({
        page_id: newPage.id,
        properties: {
          Done: {
            checkbox: true,
          },
        },
      });

      startDate = get(newPage, [
        'properties',
        NotionTasksTableKeys.StartDate,
        'date',
        'start',
      ]);
      nextDayOfWeek =
        parsedDays.find((n) => n > moment(startDate).day()) || parsedDays[0];
      nextDate = getNextWeekDay(startDate, nextDayOfWeek);

      newPage = await NotionService.notion.pages.retrieve({
        page_id: newPage.id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).format('YYYY-MM-DD'),
      ).toBe(nextDate.format('YYYY-MM-DD'));

      // Third weekday

      await NotionService.notion.pages.update({
        page_id: newPage.id,
        properties: {
          Done: {
            checkbox: true,
          },
        },
      });

      startDate = get(newPage, [
        'properties',
        NotionTasksTableKeys.StartDate,
        'date',
        'start',
      ]);
      nextDayOfWeek =
        parsedDays.find((n) => n > moment(startDate).day()) || parsedDays[0];
      nextDate = getNextWeekDay(startDate, nextDayOfWeek);

      newPage = await NotionService.notion.pages.retrieve({
        page_id: newPage.id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).format('YYYY-MM-DD'),
      ).toBe(nextDate.format('YYYY-MM-DD'));
    });

    it('should handle tasks with old start dates', async () => {
      expect.assertions(2);

      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionTasksTableKeys.StartDate]: {
            date: {
              start: moment()
                .subtract(10, 'days')
                .subtract(6, 'hours')
                .toISOString(),
              time_zone: 'America/Costa_Rica',
            },
          },
          [NotionTasksTableKeys.RecurrenceUnit]: {
            select: {
              name: 'days',
            },
          },
          [NotionTasksTableKeys.Recurrence]: {
            number: 1,
          },
        },
      });

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      const newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).format('YYYY-MM-DD'),
      ).toBe(moment().format('YYYY-MM-DD'));
    });

    it('should handle weekly by weekday recurrent tasks', async () => {
      expect.assertions(2);

      const getNextWeekDay = (startDate: Date, nextDayOfWeek: number) => {
        return moment(startDate)
          .add(7, 'days')
          .subtract(
            moment(startDate)
              .add(7, 'days')
              .subtract(nextDayOfWeek, 'days')
              .day(),
            'days',
          );
      };

      const days = [{ name: '1|Monday' }];

      const parsedDays = days.map((str) => parseInt(str.name[0]));

      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionTasksTableKeys.StartDate]: {
            date: {
              start: moment().add(1, 'day').subtract(6, 'hours').toISOString(),
              time_zone: 'America/Costa_Rica',
            },
          },
          [NotionTasksTableKeys.RecurrenceDays]: {
            multi_select: days,
          },
          [NotionTasksTableKeys.RecurrenceUnit]: {
            select: {
              name: 'weeks',
            },
          },
          [NotionTasksTableKeys.Recurrence]: {
            number: 2,
          },
        },
      });

      // First weekday

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentTasks(await service.fetchRecurrentTasks());

      let newPageResult = await NotionService.notion.databases.query({
        database_id: get(originalPage, ['parent', 'database_id']),
        filter: {
          and: [
            {
              property: NotionTasksTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      let newPage = newPageResult.results[0];

      let startDate = get(originalPage, [
        'properties',
        NotionTasksTableKeys.StartDate,
        'date',
        'start',
      ]);
      let nextDayOfWeek =
        parsedDays.find((n) => n > moment(startDate).day()) || parsedDays[0];
      let nextDate = getNextWeekDay(startDate, nextDayOfWeek).add(1, 'week');

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionTasksTableKeys.StartDate,
            'date',
            'start',
          ]),
        ).format('YYYY-MM-DD'),
      ).toBe(nextDate.format('YYYY-MM-DD'));
    });
  });

  describe('Budget Tasks Database', () => {
    beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [AppEnv],
        providers: [NotionService],
      }).compile();

      service = module.get<NotionService>(NotionService);
      database_id = module.get<string>(NOTION_TRANSACTION_CATEGORY_DATABASE_ID);

      const data = await service.createPage({
        parent: { database_id },
        properties: {
          [NotionCategoryTableKeys.PaymentDate]: {
            type: 'date',
            date: {
              start: moment().format('YYYY-MM-DD'),
            },
          },
          [NotionCategoryTableKeys.RecurrenceUnit]: {
            type: 'select',
            select: null,
          },
          [NotionCategoryTableKeys.Recurrence]: {
            type: 'number',
            number: 9,
          },
          [NotionCategoryTableKeys.Name]: {
            type: 'title',
            title: [
              {
                text: {
                  content: 'notion-watchers',
                },
              },
            ],
          },
        },
      } as any);

      page_id = data.id;
    });

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [AppEnv],
        providers: [NotionService],
      }).compile();

      service = module.get<NotionService>(NotionService);

      // Reset task status
      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionCategoryTableKeys.PaymentDate]: {
            date: {
              start: moment().format('YYYY-MM-DD'),
            },
          },
          [NotionCategoryTableKeys.Paid]: {
            checkbox: true,
          },
          [NotionCategoryTableKeys.Scheduled]: {
            checkbox: false,
          },
          [NotionCategoryTableKeys.RecurrenceUnit]: {
            select: {
              name: 'days',
            },
          },
          [NotionCategoryTableKeys.Recurrence]: {
            number: 2,
          },
          [NotionCategoryTableKeys.Prev]: {
            relation: [],
          },
          [NotionCategoryTableKeys.Next]: {
            relation: [],
          },
        },
      });
    });

    afterEach(async () => {
      if (!clean) return;
      const results = await NotionService.notion.databases.query({
        database_id,
        filter: {
          and: [
            {
              property: NotionCategoryTableKeys.Name,
              title: {
                starts_with: 'notion-watchers',
              },
            },
          ],
        },
      });

      for (let page of createdPagesIds) {
        await NotionService.notion.pages.update({
          page_id: page,
          archived: true,
        });
      }

      for (let result of results.results) {
        if (result.id === page_id) break;

        await NotionService.notion.pages.update({
          page_id: result.id,
          archived: true,
        });
      }
    });

    afterAll(async () => {
      if (!clean) return;
      await NotionService.notion.pages.update({
        page_id,
        archived: true,
      });
    });

    it('should duplicate financial tasks', async () => {
      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionCategoryTableKeys.Clone]: {
            checkbox: true,
          },
        },
      });

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentFinancialTasks(
        await service.fetchRecurrentFinanceTasks(),
      );

      const newPageResult = await NotionService.notion.databases.query({
        database_id,
        filter: {
          and: [
            {
              property: NotionCategoryTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
            {
              property: NotionCategoryTableKeys.Next,
              relation: {
                is_empty: true,
              },
            },
            {
              property: NotionCategoryTableKeys.Prev,
              relation: {
                contains: originalPage.id,
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionCategoryTableKeys.PaymentDate,
            'date',
            'start',
          ]),
        ).toISOString(),
      ).toEqual(
        moment(
          get(originalPage, [
            'properties',
            NotionCategoryTableKeys.PaymentDate,
            'date',
            'start',
          ]),
        )
          .add(
            get(originalPage, [
              'properties',
              NotionCategoryTableKeys.Recurrence,
              'number',
            ]),
            get(originalPage, [
              'properties',
              NotionCategoryTableKeys.RecurrenceUnit,
              'select',
              'name',
            ]),
          )
          .toISOString(),
      );
    });

    it('should handle daily recurrent financial tasks', async () => {
      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionCategoryTableKeys.PaymentDate]: {
            date: {
              start: moment().subtract(3, 'days').format('YYYY-MM-DD'),
            },
          },
        },
      });

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentFinancialTasks(
        await service.fetchRecurrentFinanceTasks(),
      );

      const newPageResult = await NotionService.notion.databases.query({
        database_id,
        filter: {
          and: [
            {
              property: NotionCategoryTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
            {
              property: NotionCategoryTableKeys.Next,
              relation: {
                is_empty: true,
              },
            },
            {
              property: NotionCategoryTableKeys.Prev,
              relation: {
                contains: originalPage.id,
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage = newPageResult.results[0];

      expect(
        moment(
          get(newPage, [
            'properties',
            NotionCategoryTableKeys.PaymentDate,
            'date',
            'start',
          ]),
        ).toISOString(),
      ).toEqual(
        moment(
          get(originalPage, [
            'properties',
            NotionCategoryTableKeys.PaymentDate,
            'date',
            'start',
          ]),
        )
          .add(
            get(originalPage, [
              'properties',
              NotionCategoryTableKeys.Recurrence,
              'number',
            ]),
            get(originalPage, [
              'properties',
              NotionCategoryTableKeys.RecurrenceUnit,
              'select',
              'name',
            ]),
          )
          .toISOString(),
      );
    });

    it('should handle daily recurrent financial tasks when multiple task ahead exists', async () => {
      await NotionService.notion.pages.update({
        page_id,
        properties: {
          [NotionCategoryTableKeys.Paid]: {
            checkbox: false,
          },
          [NotionCategoryTableKeys.Clone]: {
            checkbox: true,
          },
        },
      });

      const originalPage = await NotionService.notion.pages.retrieve({
        page_id,
      });

      await service.processRecurrentFinancialTasks(
        await service.fetchRecurrentFinanceTasks(),
      );

      let newPageResult = await NotionService.notion.databases.query({
        database_id,
        filter: {
          and: [
            {
              property: NotionCategoryTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
            {
              property: NotionCategoryTableKeys.Next,
              relation: {
                is_empty: true,
              },
            },
            {
              property: NotionCategoryTableKeys.Prev,
              relation: {
                contains: originalPage.id,
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage = newPageResult.results[0];

      await NotionService.notion.pages.update({
        page_id: newPage.id,
        properties: {
          [NotionCategoryTableKeys.Clone]: {
            checkbox: true,
          },
        },
      });

      await service.processRecurrentFinancialTasks(
        await service.fetchRecurrentFinanceTasks(),
      );

      newPageResult = await NotionService.notion.databases.query({
        database_id,
        filter: {
          and: [
            {
              property: NotionCategoryTableKeys.Name,
              title: {
                equals: 'notion-watchers',
              },
            },
            {
              property: NotionCategoryTableKeys.Next,
              relation: {
                is_empty: true,
              },
            },
            {
              property: NotionCategoryTableKeys.Prev,
              relation: {
                contains: newPage.id,
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);

      const newPage2 = newPageResult.results[0];

      expect(
        moment(
          get(newPage2, [
            'properties',
            NotionCategoryTableKeys.PaymentDate,
            'date',
            'start',
          ]),
        ).toISOString(),
      ).toEqual(
        moment(
          get(originalPage, [
            'properties',
            NotionCategoryTableKeys.PaymentDate,
            'date',
            'start',
          ]),
        )
          .add(
            parseInt(
              get(originalPage, [
                'properties',
                NotionCategoryTableKeys.Recurrence,
                'number',
              ]),
              10,
            ) * 2,
            get(originalPage, [
              'properties',
              NotionCategoryTableKeys.RecurrenceUnit,
              'select',
              'name',
            ]),
          )
          .toISOString(),
      );
    });
  });

  describe('Budget Transactions Database', () => {
    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        imports: [AppEnv],
        providers: [NotionService],
      }).compile();

      service = module.get<NotionService>(NotionService);
      database_id = module.get<string>(NOTION_TRANSACTIONS_DATABASE_ID);
    });

    afterEach(async () => {
      const results = await NotionService.notion.databases.query({
        database_id,
        filter: {
          and: [
            {
              property: NotionTransactionsTableKeys.Name,
              title: {
                starts_with: 'notion-watchers',
              },
            },
          ],
        },
      });

      for (let page of createdPagesIds) {
        await NotionService.notion.pages.update({
          page_id: page,
          archived: true,
        });
      }

      for (let result of results.results) {
        if (result.id === page_id) break;

        await NotionService.notion.pages.update({
          page_id: result.id,
          archived: true,
        });
      }
    });

    it('should process results and create them in notion', async () => {
      const responses = await service.processTransactions([
        {
          index: 1,
          ref: 1234,
          date: new Date(),
          account: 1234,
          detail: 'AMAZON',
          debit: 100,
          currency: 'USD',
          type: 'CP',
          balance: 100,
        },
      ]);

      if (responses.length) {
        createdPagesIds.push(...responses.map((response) => response.id));
      }

      const newPageResult = await NotionService.notion.databases.query({
        database_id,
        filter: {
          and: [
            {
              property: NotionTransactionsTableKeys.Name,
              title: {
                equals: 'AMAZON - 1234',
              },
            },
          ],
        },
      });

      expect(newPageResult.results).toHaveLength(1);
    });
  });
});
