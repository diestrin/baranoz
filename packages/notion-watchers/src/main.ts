import * as morgan from 'morgan';
import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';

import { AppModule } from './app.module';
import { SentryService } from '@ntegral/nestjs-sentry';

const logger = new Logger('main');

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: false });

  app.use(
    morgan('dev', {
      skip: function (req, res) {
        if (req.url == '/healthz') {
          return true;
        } else {
          return false;
        }
      },
    }),
  );

  process.on('SIGINT', async () => {
    setTimeout(() => process.exit(1), 5000);
    await app.close();
    process.exit(0);
  });

  // kill -15
  process.on('SIGTERM', async () => {
    setTimeout(() => process.exit(1), 5000);
    await app.close();
    process.exit(0);
  });

  app.useLogger(SentryService.SentryServiceInstance());
  await app.listen(3000);
  logger.log('App listening on port 3000');
}
bootstrap();
