import { JSDOM } from 'jsdom';
import { get } from 'lodash';
import * as moment from 'moment';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { gmail, auth, gmail_v1 } from '@googleapis/gmail';

import {
  GMAIL_CLIENT_ID,
  GMAIL_CLIENT_SECRET,
  GMAIL_REDIRECT_URL,
  GMAIL_REFRESH_TOKEN,
} from '../env';
import { Transaction } from 'src/types';

@Injectable()
export class GmailService {
  private static logger = new Logger(GmailService.name);

  public gmail: gmail_v1.Gmail;

  constructor(
    @Inject(GMAIL_CLIENT_ID) private gmailClientId: string,
    @Inject(GMAIL_CLIENT_SECRET) private gmailClientSecret: string,
    @Inject(GMAIL_REDIRECT_URL) private gmailRedirectUrl: string,
    @Inject(GMAIL_REFRESH_TOKEN) private gmailRefreshToken: string,
  ) {}

  getAuthUrl() {
    const authClient = new auth.OAuth2(
      this.gmailClientId,
      this.gmailClientSecret,
      this.gmailRedirectUrl,
    );

    return authClient.generateAuthUrl({
      access_type: 'offline',
      scope: ['https://mail.google.com/'],
    });
  }

  async authenticate(authoCode?: string) {
    const authClient = new auth.OAuth2(
      this.gmailClientId,
      this.gmailClientSecret,
      this.gmailRedirectUrl,
    );

    let refresh_token: string;
    if (authoCode) {
      const { tokens } = await authClient.getToken(authoCode);
      authClient.setCredentials(tokens);

      if (tokens.refresh_token) {
        refresh_token = tokens.refresh_token;
      }
    } else if (this.gmailRefreshToken) {
      authClient.setCredentials({ refresh_token: this.gmailRefreshToken });
    } else {
      throw new Error('No refresh token');
    }

    authClient.on('tokens', async (tokens) => {
      if (tokens.refresh_token) {
        GmailService.logger.log('Refreshing tokens');
        await this.backupRefreshToken(tokens.refresh_token);
      }

      authClient.setCredentials(tokens);
    });

    this.gmail = gmail({
      version: 'v1',
      auth: authClient,
    });

    if (refresh_token) {
      await this.backupRefreshToken(refresh_token);
    }
  }

  async fetchTransactions() {
    await this.authenticate();

    const messages = await this.gmail.users.messages.list({
      userId: 'me',
      q: 'label:transaction label:unread',
    });

    GmailService.logger.log(`Got ${messages.data?.messages?.length} messages`);

    const transactions: Transaction[] = [];

    if (!messages.data?.messages?.length) {
      return transactions;
    }

    for (let message of messages.data.messages) {
      const firstMessage = await this.gmail.users.messages.get({
        userId: 'me',
        id: message.id,
      });

      let emailData: string = '';
      for (let part of firstMessage.data.payload.parts || [
        firstMessage.data.payload,
      ]) {
        let data = part.body.data;

        const innerParts = get(part, 'parts', []);
        if (!data && innerParts) {
          for (let innerPart of innerParts) {
            if (!innerPart.body.data) {
              continue;
            }

            if (innerPart.mimeType !== 'text/html') {
              continue;
            }

            data = innerPart.body.data;
            break;
          }
        } else if (!data) {
          continue;
        }

        emailData = data;
        break;
      }

      if (!emailData) {
        GmailService.logger.log(`Message ${message.id} has no data, skipping`);
        continue;
      }

      const data = Buffer.from(emailData, 'base64url').toString('utf-8');
      const doc = new JSDOM(data);

      const store = Array.from(doc.window.document.querySelectorAll('p')).find(
        (p) => p.textContent.includes('Comercio:'),
      );
      const mainTable: HTMLElement = (function findTable(el?: HTMLElement) {
        return !el.parentElement
          ? null
          : /table/i.test(el.parentElement.tagName)
          ? el
          : findTable(el.parentElement);
      })(store);

      if (!mainTable) {
        GmailService.logger.log(`Message ${message.id} has no table, skipping`);
        continue;
      }

      const transaction = Array.from(mainTable.querySelectorAll('tr')).reduce(
        (result, row) => {
          const cells = [
            ...Array.from(row.querySelectorAll('th') || []),
            ...Array.from(row.querySelectorAll('td') || []),
          ];

          if (cells.length === 2) {
            const key = cells[0].textContent.trim().replace(/:$/, '');
            const value = cells[1].textContent.trim();

            result[key] = value;
          } else {
            GmailService.logger.log(
              `Cannot find key/value pair in row: ${row.innerHTML}`,
            );
          }

          return result;
        },
        {} as any,
      );

      const locale = moment.locale();
      moment.locale('es');
      const tx = {
        ref: parseInt(transaction.Referencia, 10) || null,
        authorization: parseInt(transaction['Autorización'], 10) || null,
        location: transaction['Ciudad y país'],
        date: transaction.Fecha.replace(
          /^((\w+) (\d+),? ?(\d+),? .*)/,
          (_, $_, $1, $2, $3) =>
            `${
              moment
                .months()
                .findIndex((mo) => mo.startsWith($1.toLowerCase())) + 1
            }/${$2}/${`20${$3}`.slice(-4)}`,
        ),
        account: transaction.AMEX
          ? `AMEX:${transaction.AMEX}`
          : transaction.VISA
          ? `VISA:${transaction.VISA}`
          : transaction.Mastercard
          ? `Mastercard:${transaction.Mastercard}`
          : 'Unknown',
        store: transaction.Comercio,
        amount: parseFloat(transaction.Monto.replace(/[^\d.]/g, '')),
        currency: transaction.Monto.slice(0, 3),
        type: transaction['Tipo de Transacción'],
      };
      moment.locale(locale);
      GmailService.logger.log(`Formatted tx ${JSON.stringify(tx)}`);
      // transactions.push(tx);

      await this.gmail.users.messages.modify({
        userId: 'me',
        id: message.id,
        requestBody: {
          removeLabelIds: ['UNREAD'],
        },
      });
    }

    return transactions;
  }

  private async backupRefreshToken(token: string) {
    const userProfile = await this.gmail.users.getProfile({
      userId: 'me',
    });
    GmailService.logger.log(
      `Backing up token in ${userProfile.data.emailAddress}`,
    );

    await this.gmail.users.messages.insert({
      userId: 'me',
      requestBody: {
        raw: Buffer.from(
          [
            'From: baranoz@localpower.diegobarahona.com',
            `To: ${userProfile.data.emailAddress}`,
            'Content-Type: text/plain; charset=utf-8',
            'MIME-Version: 1.0',
            'Subject: Refresh token',
            '',
            `${token}`,
          ].join('\n'),
        )
          .toString('base64')
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/=+$/, ''),
      },
    });
  }
}
