import { Test, TestingModule } from '@nestjs/testing';
import { gmail, auth } from '@googleapis/gmail';

import {
  AppEnv,
  GMAIL_CLIENT_ID,
  GMAIL_CLIENT_SECRET,
  GMAIL_REDIRECT_URL,
  NOTION_TRANSACTIONS_DATABASE_ID,
} from '../env';

import { GmailService } from './gmail.service';
import { NotionService } from '../notion/notion.service';

jest.setTimeout(1000 * 60 * 10);

describe('NotionService', () => {
  let gmailService: GmailService;
  let notionService: NotionService;
  let database_id: string;
  const createdPagesIds = [];

  let gmailClientId: string;
  let gmailRedirectUrl: string;
  let gmailClientSecret: string;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppEnv],
      providers: [GmailService, NotionService],
    }).compile();

    gmailService = module.get<GmailService>(GmailService);
    notionService = module.get<NotionService>(NotionService);
    database_id = module.get<string>(NOTION_TRANSACTIONS_DATABASE_ID);

    gmailClientId = module.get<string>(GMAIL_CLIENT_ID);
    gmailClientSecret = module.get<string>(GMAIL_CLIENT_SECRET);
    gmailRedirectUrl = module.get<string>(GMAIL_REDIRECT_URL);

    const app = module.createNestApplication();
    await app.init();
    app.useLogger(console);
  });

  afterEach(async () => {
    for (let page of createdPagesIds) {
      await NotionService.notion.pages.update({
        page_id: page,
        archived: true,
      });
    }
  });

  it.skip('should query for emails and find them', async () => {
    const transactions = await gmailService.fetchTransactions();
    expect(transactions.length).toBeGreaterThan(0);
  });

  it.skip('should migrate emails from account A to account B', async () => {
    const authClient = new auth.OAuth2(
      gmailClientId,
      gmailClientSecret,
      gmailRedirectUrl,
    );
    authClient.setCredentials({
      refresh_token:
        '1//05m9MdK-TZ0d4CgYIARAAGAUSNwF-L9IrwjbMg510dihhqxogzCVVIp-iNvJDYo8ME40Y1yvVWo88HY_SNNPgNdFh7D02n3ly6x8',
    });
    const gmailClient = gmail({
      version: 'v1',
      auth: authClient,
    });
    const messages = await gmailClient.users.messages.list({
      userId: 'me',
      q: 'label:bac-updates label:unread',
    });

    if (!messages.data?.messages?.length) {
      console.log('No messages found');
      return;
    }

    await gmailService.authenticate();
    for (let message of messages.data?.messages) {
      const firstMessage = await gmailClient.users.messages.get({
        userId: 'me',
        id: message.id,
      });

      await gmailService.gmail.users.messages.insert({
        userId: 'me',
        requestBody: {
          raw: Buffer.from(
            [
              ...firstMessage.data.payload.headers
                .filter((h) => !['To', 'Content-Type'].includes(h.name))
                .map((h) => `${h.name}: ${h.value}`),
              `To: armando.aruga@gmail.com`,
              'Content-Type: text/html; charset=utf-8',
              '',
              `${Buffer.from(
                firstMessage.data.payload.parts[0].body.data,
                'base64url',
              ).toString()}`,
            ].join('\n'),
          )
            .toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=+$/, ''),
          labelIds: ['UNREAD'],
        },
      });

      await gmailClient.users.messages.modify({
        userId: 'me',
        id: message.id,
        requestBody: {
          removeLabelIds: ['UNREAD'],
        },
      });
    }
  });
});
