import { Module } from '@nestjs/common';
import { SentryModule } from '@ntegral/nestjs-sentry';

import { AppInfoService } from './app-info.service';

import { AppEnv, SENTRY_DSN_URL } from './env';
import { AppController } from './app.controller';
import { GmailService } from './gmail/gmail.service';
import { NotionService } from './notion/notion.service';
import { BacService } from './bac/bac.service';

@Module({
  imports: [
    AppEnv,
    SentryModule.forRootAsync({
      imports: [AppEnv],
      useFactory: (sentryDsnUrl: string) => ({
        dsn: sentryDsnUrl,
        debug: true,
      }),
      inject: [SENTRY_DSN_URL],
    }),
  ],
  controllers: [AppController],
  providers: [AppInfoService, NotionService, GmailService, BacService],
})
export class AppModule {}
