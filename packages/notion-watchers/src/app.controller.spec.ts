import { Test, TestingModule } from '@nestjs/testing';

import { AppController } from './app.controller';
import { AppInfoService } from './app-info.service';
import { NotionService } from './notion/notion.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppInfoService, NotionService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('healthz', () => {
    it('should return app name', () => {
      expect(appController.health().name).toBe(
        '@diestrin/baranoz-notion-watchers',
      );
    });
  });
});
