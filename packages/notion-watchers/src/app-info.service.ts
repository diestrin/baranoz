import { Injectable } from '@nestjs/common';

const pkgJson = require('../package.json');

@Injectable()
export class AppInfoService {
  getStatus() {
    return {
      name: pkgJson.name,
      host: process.env.HOSTNAME,
      version: pkgJson.version,
      sha1: process.env.GIT_REVISION,
      uptime: +process.uptime() + 's',
    };
  }
}
