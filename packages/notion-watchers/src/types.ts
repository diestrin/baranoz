export interface Transaction {
  index: number;
  ref: number;
  date: Date;
  account: number;
  detail: string;
  debit?: number;
  credit?: number;
  currency: string;
  type: string;
  exchangeRate?: number;
  balance: number;
  destiny?: string;
  destinyAmount?: number;
  destinyCurrency?: string;
  message?: string;
}
