import {
  Body,
  Controller,
  Get,
  Header,
  Headers,
  Logger,
  Post,
  Query,
} from '@nestjs/common';
import { CloudEventV1, HTTP } from 'cloudevents';

import { BacService } from './bac/bac.service';
import { AppInfoService } from './app-info.service';
import { GmailService } from './gmail/gmail.service';
import { NotionService } from './notion/notion.service';

@Controller()
export class AppController {
  private static logger = new Logger(AppController.name);

  constructor(
    private readonly appInfoService: AppInfoService,
    private readonly notionService: NotionService,
    private readonly gmailService: GmailService,
    private readonly bacService: BacService,
  ) {}

  @Get('/healthz')
  health() {
    return this.appInfoService.getStatus();
  }

  @Get('/gmail-auth')
  @Header('Content-Type', 'text/html')
  gmailAuth() {
    return `
      <a href="${this.gmailService.getAuthUrl()}">Login</a>
    `;
  }

  @Get('/gmail-auth-callback')
  async gmailAuthCallback(@Query('code') code: string) {
    await this.gmailService.authenticate(code);

    return {
      ok: true,
    };
  }

  @Post()
  async message(@Body() body: object, @Headers() headers) {
    const receivedEvent = HTTP.toEvent({
      headers,
      body,
    }) as CloudEventV1<unknown>;

    console.log('Hnadling event:', receivedEvent.subject);
    switch (receivedEvent.subject) {
      case 'fetch-tasks': {
        const tasks = await this.notionService.fetchRecurrentTasks();
        await this.notionService.processRecurrentTasks(tasks);
        break;
      }
      case 'fetch-transactions': {
        const transactions = await this.bacService.fetchTransactions();
        await this.notionService.processTransactions(transactions);
        break;
      }
      case 'fetch-transaction-tasks': {
        const tasks = await this.notionService.fetchRecurrentFinanceTasks();
        await this.notionService.processRecurrentFinancialTasks(tasks);
        break;
      }
    }
  }
}
