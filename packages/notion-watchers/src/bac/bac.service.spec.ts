import * as moment from 'moment';
import { Test, TestingModule } from '@nestjs/testing';

import { AppEnv, NOTION_TRANSACTIONS_DATABASE_ID } from '../env';

import { NotionService } from '../notion/notion.service';
import { BacService } from './bac.service';

jest.setTimeout(1000 * 60 * 10);

describe('BacService', () => {
  let bacService: BacService;
  let notionService: NotionService;
  let database_id: string;
  const createdPagesIds = [];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppEnv],
      providers: [BacService, NotionService],
    }).compile();

    bacService = module.get<BacService>(BacService);
    notionService = module.get<NotionService>(NotionService);
    database_id = module.get<string>(NOTION_TRANSACTIONS_DATABASE_ID);

    const app = module.createNestApplication();
    await app.init();
    app.useLogger(console);
  });

  afterEach(async () => {
    for (let page of createdPagesIds) {
      await NotionService.notion.pages.update({
        page_id: page,
        archived: true,
      });
    }
  });

  it('should scrap for transactions', async () => {
    const transactions = await bacService.fetchTransactions({
      bacLastQueryDate: moment('2023-07-01').toDate(),
      bacQueryDays: 1,
    });
    expect(transactions.length).toBeGreaterThan(0);
    console.log(JSON.stringify(transactions));
    // await notionService.processTransactions(transactions);
  });
});
