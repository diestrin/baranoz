import * as moment from 'moment';

export const userName = process.env.BAC_USERNAME || '';
console.log('BAC_USERNAME', userName);

export const password = process.env.BAC_PASSWORD || '';
console.log(
  'BAC_PASSWORD',
  password
    .split('')
    .map((x) => '*')
    .join(''),
);

export const bacLastQueryDate = moment(
  process.env.BAC_LAST_QUERY_DATE || Date.now(),
).toDate();
console.log('BAC_LAST_QUERY_DATE', bacLastQueryDate);

export const bacQueryDays = parseInt(process.env.BAC_QUERY_DAYS || '1', 10);
console.log('BAC_QUERY_DAYS', bacQueryDays);

export const testEnv = process.env.TEST_ENV || 'test';
console.log('TEST_ENV', testEnv);
