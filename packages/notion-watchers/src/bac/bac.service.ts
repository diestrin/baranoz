import * as moment from 'moment';
import { defaults } from 'lodash';
import { fork } from 'child_process';
import { Inject, Injectable, Logger } from '@nestjs/common';

import { Transaction } from '../types';
import { NotionService, NotionSettings } from '../notion/notion.service';
import { testEnv } from './env';

@Injectable()
export class BacService {
  private static logger = new Logger(BacService.name);

  constructor(@Inject(NotionService) private notionService: NotionService) {}

  async fetchTransactions(options: Partial<NotionSettings> = {}) {
    const settings = defaults(options, await this.notionService.getSettings());

    if (
      moment(settings.bacLastQueryDate)
        .subtract(1, 'days')
        .isSameOrAfter(moment())
    ) {
      BacService.logger.warn(
        `BAC query date is in the future. Skipping query.`,
      );
      return [];
    }

    const transactions = await new Promise<Transaction[]>((resolve, reject) => {
      let replied = false;
      const nightwatchConfig = require.resolve(
        '../../dist/bac/nightwatch.conf',
      );
      const bacWorker = require.resolve('../../dist/bac/bac.worker');

      BacService.logger.log(
        `About to fork ${bacWorker} with config ${nightwatchConfig}`,
      );
      const child = fork(
        require.resolve('nightwatch/bin/nightwatch'),
        ['-c', nightwatchConfig, '--env', testEnv, bacWorker],
        {
          stdio: [null, process.stdout, process.stderr, 'ipc'],
          env: {
            ...process.env,
            BAC_LAST_QUERY_DATE: moment(settings.bacLastQueryDate).format(
              'YYYY-MM-DD',
            ),
            BAC_QUERY_DAYS: settings.bacQueryDays.toString(),
          },
        },
      );

      child.on('spawn', () => {
        BacService.logger.log(`Child started`);
      });

      child.on('message', (data: any[]) => {
        BacService.logger.log(`Received ${data.length} transactions`);
        resolve(data);
        replied = true;
      });

      child.on('error', (err) => {
        BacService.logger.error(err);
        reject(err);
      });

      child.on('exit', (code: number, signal: any) => {
        if (!replied) {
          BacService.logger.log(`Child terminated with ${code} and ${signal}`);
          reject(`Child terminated with ${code} and ${signal}`);
        }
      });
    });

    await this.notionService.setSettings({
      bacLastQueryDate: moment(settings.bacLastQueryDate)
        .add(settings.bacQueryDays - 1, 'days')
        .toDate(),
    });

    return transactions;
  }
}
