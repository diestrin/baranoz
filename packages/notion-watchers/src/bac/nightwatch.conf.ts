const chromedriver = require('chromedriver');

const filename_format = function ({
  testSuite = '',
  testCase = '',
  isError = false,
  dateObject = new Date(),
} = {}) {
  const fileName = [];
  const dateParts = dateObject.toString().replace(/:/g, '').split(' ');
  dateParts.shift();

  const dateStamp = dateParts.slice(0, 5).join('-');
  if (testSuite) {
    fileName.push(testSuite);
  }
  if (testCase) {
    fileName.push(testCase);
  }

  return `${fileName.join('/')}${
    isError ? '_ERROR' : '_FAILED'
  }_${dateStamp}.png`;
};

export = {
  enable_fail_fast: true,

  src_folders: [],

  test_workers: false,

  // webdriver: {
  //   start_process: true,
  //   //chromedriver.path || '/usr/bin/chromedriver',
  //   // server_path: require.resolve('chromedriver/bin/chromedriver'),
  //   // port: 9515,
  // },

  screenshots: {
    enabled: true,
    filename_format,
    path: '',
    on_error: true,
    on_failure: true,
  },

  test_settings: {
    default: {
      desiredCapabilities: {
        browserName: 'chrome',
        'goog:chromeOptions': {
          args: [
            // '--no-sandbox',
            '--disable-dev-shm-usage',
            //'--ignore-certificate-errors',
            //'--allow-insecure-localhost',
            '--headless',
          ],
        },
      },
    },
    cluster: {
      webdriver: {
        start_process: false,
        host: 'selenium-hub',
        port: 4444,
      },
    },
    test: {
      webdriver: {
        start_process: true,
        port: 9515,
        server_path: require.resolve('chromedriver/bin/chromedriver'),
      },
    },
  },
};
