import { NightwatchBrowser } from 'nightwatch';
import * as moment from 'moment';
import { Transaction } from 'src/types';

import { bacLastQueryDate, bacQueryDays, password, userName } from './env';

const urls = {
  login: 'https://www1.sucursalelectronica.com/redir/showLogin.go',
  logout: 'https://www1.sucursalelectronica.com/ebac/common/logout.go',
};

const selectors = {
  login: {
    username: '#productId',
    password: '#pass',
    submit: '#confirm',
  },
  dashboard: {
    accounts: '#referenceDiv + div [id^=tbodyproductTable] tr',
    account: '#referenceDiv + div [id^=tbodyproductTable] tr:nth-child(1)',
    accountName: 'td:nth-child(1)',
    accountIban: 'td:nth-child(2)',
    accountBalance: 'td:nth-child(4)',
    accountDetailsButton: '[id^=tdAccountBalanceItem] button',
    creditCards:
      '[id^=productTableCreditCard] [id^=tbodyproductTableCreditCard] tr',
    creditCard:
      '[id^=productTableCreditCard] [id^=tbodyproductTableCreditCard] tr:nth-of-type(1)',
    creditCardName: 'td:nth-child(1)',
    creditCardAccount: 'td:nth-child(2)',
    creditCardBalanceCRC: 'td:nth-child(3) li:nth-child(1)',
    creditCardBalanceUSD: 'td:nth-child(3) li:nth-child(2)',
    creditCardDetailsButton: '[id^=tdAccountBalanceItem] button',
    exchangeRateBuy: '#currencyBuy',
    exchangeRateSell: '#currencySale',
  },
  creditCardDetails: {
    recentMovements: '#tab-transactions',
    monthSelect: '#calendarSelectDiv',
    monthSelectItems: '#calendarSelectDiv li',
    monthSelectItem: '#calendarSelectDiv li:nth-child(2)',
    recentTransactionsTable: '#tbodycreditCardRecentMovementsTable',
    monthTransactionsTable: '#tbodycreditCardStateTRX',
    transactions: 'tr',
    transaction: 'tr:nth-child(3)',
    transactionDate: 'td:nth-child(1)',
    transactionName: 'td:nth-child(2)',
    transactionDebtCRC: 'td:nth-child(3)',
    transactionDebtUSD: 'td:nth-child(4)',
  },
  accountDetails: {
    balance: '.main-amount',
    advancedSearch: '#adv-search',
    initDateFilter: '#initDate',
    endDateFilter: '#endDate',
    filterButton: '#advancedSearchContainer .bel-btn-default-active',
    transactions: '#transactionTable1 tbody tr',
    transaction: '#transactionTable1 tbody tr:nth-of-type(2)',
    transactionDate: 'td:nth-of-type(1)',
    transactionRef: 'td:nth-of-type(2)',
    transactionName: 'td:nth-of-type(3)',
    transactionDebt: 'td:nth-of-type(4)',
    transactionCrdt: 'td:nth-of-type(5)',
    transactionBalance: 'td:nth-of-type(6)',
  },
  transferDetails: {
    transferDetailsContainer: '#mainContentTable:only-child',
    exchangeRate:
      '#mainContentTable tr:nth-child(3) td #resultsTable tr:nth-child(3) td:nth-child(2)',
    creeditAmount:
      '#mainContentTable tr:nth-child(7) td #resultsTable tr:nth-child(2) td:nth-child(2)',
    description:
      '#mainContentTable tr:nth-child(7) td #resultsTable tr:nth-child(3) td:nth-child(2)',
    backButton: 'div + #mainContentTable table td:nth-child(1) a',
  },
};

const login = async (browser: NightwatchBrowser) => {
  await (browser as any).chrome.sendDevToolsCommand('Network.enable');
  await (browser as any).chrome.sendDevToolsCommand(
    'Network.setExtraHTTPHeaders',
    {
      headers: {
        'Accept-Language': 'en-US,en;q=0.9,es;q=0.8',
        Connection: 'keep-alive',
        'User-Agent':
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36',
      },
    },
  );
  await browser.url(urls.login);

  await browser.waitForElementVisible(selectors.login.username);
  await browser.setValue(selectors.login.username, userName);

  await browser.waitForElementVisible(selectors.login.password);
  await browser.setValue(selectors.login.password, password);

  await browser.waitForElementVisible(selectors.login.submit);
  await browser.click(selectors.login.submit);

  const url: string = (await browser.url()) as any;

  if (/showSessionRestriction\.go/.test(url)) {
    await browser.waitForElementVisible('.button-position-right');
    await browser.click('.button-position-right');
  }

  await browser.waitForElementVisible(selectors.dashboard.accounts);
};

const logout = async (browser: NightwatchBrowser) => {
  await browser.url(urls.logout);
  await browser.end();
};

const getAccountDetails = async (
  browser: NightwatchBrowser,
  accountSelector: string,
) => {
  await browser.waitForElementVisible(accountSelector);

  const name = await getString(
    browser,
    `${accountSelector} ${selectors.dashboard.accountName}`,
  );
  const iban = parseInt(
    (
      await getString(
        browser,
        `${accountSelector} ${selectors.dashboard.accountIban}`,
      )
    ).replace(/^[a-z]+/i, ''),
    10,
  );
  const balance = await getNumber(
    browser,
    `${accountSelector} ${selectors.dashboard.accountBalance}`,
  );
  const currency = ((
    await getString(
      browser,
      `${accountSelector} ${selectors.dashboard.accountBalance}`,
    )
  ).match(/[A-Z]{3}/) || ['UKN'])[0];

  return { name, iban, balance, currency };
};

const getCreditCardDetails = async (
  browser: NightwatchBrowser,
  creditCardSelector: string,
) => {
  await browser.waitForElementVisible(creditCardSelector);

  const name = await getString(
    browser,
    `${creditCardSelector} ${selectors.dashboard.creditCardName}`,
  );
  const account = parseInt(
    (
      await getString(
        browser,
        `${creditCardSelector} ${selectors.dashboard.creditCardAccount}`,
      )
    ).replace(/^.*(\d{4})$/i, '$1'),
  );
  const balanceCRC = await getNumber(
    browser,
    `${creditCardSelector} ${selectors.dashboard.creditCardBalanceCRC}`,
  );
  const balanceUSD = await getNumber(
    browser,
    `${creditCardSelector} ${selectors.dashboard.creditCardBalanceUSD}`,
  );

  return { name, account, balanceCRC, balanceUSD };
};

const navigateToAccountDetails = async (
  browser: NightwatchBrowser,
  accountSelector: string,
) => {
  await browser.waitForElementVisible(
    `${accountSelector} ${selectors.dashboard.accountDetailsButton}`,
  );
  await browser.click(
    `${accountSelector} ${selectors.dashboard.accountDetailsButton}`,
  );

  await browser.waitForElementVisible(selectors.accountDetails.advancedSearch);
  await browser.click(selectors.accountDetails.advancedSearch);
};

const navigateToCreditCardDetails = async (
  browser: NightwatchBrowser,
  accountSelector: string,
) => {
  await browser.waitForElementVisible(
    `${accountSelector} ${selectors.dashboard.creditCardDetailsButton}`,
  );
  await browser.click(
    `${accountSelector} ${selectors.dashboard.creditCardDetailsButton}`,
  );

  await browser.waitForElementVisible(
    selectors.creditCardDetails.recentMovements,
    10000,
  );
  await browser.click(selectors.creditCardDetails.recentMovements);
  await browser.waitForElementVisible(
    `${selectors.creditCardDetails.recentTransactionsTable} ${selectors.creditCardDetails.transactions}`,
  );
};

const filterByDate = async (
  browser: NightwatchBrowser,
  initDate: Date,
  endDate: Date = initDate,
) => {
  const initDateFormdatted = moment(initDate).format('DD/MM/YYYY');
  const endDateFormdatted = moment(endDate).format('DD/MM/YYYY');

  await browser.setAttribute(
    selectors.accountDetails.initDateFilter,
    'value',
    initDateFormdatted,
  );
  await browser.setAttribute(
    selectors.accountDetails.endDateFilter,
    'value',
    endDateFormdatted,
  );

  await browser.click(selectors.accountDetails.filterButton);
  await browser.waitForElementVisible(selectors.accountDetails.transaction);
};

const getString = async (browser: NightwatchBrowser, selector: string) =>
  (await browser.getText(selector)) as any as string;

const getNumber = async (browser: NightwatchBrowser, selector: string) =>
  parseFloat((await getString(browser, selector)).replace(/,/g, ''));

const getDate = async (browser: NightwatchBrowser, selector: string) => {
  const dateString = await getString(browser, selector);
  return moment(dateString, 'DD/MM/YYYY').toDate();
};

const getTransactions = async (
  browser: NightwatchBrowser,
  { iban, currency }: { iban: number; currency: string },
  fromDate: Date,
  toDate: Date = fromDate,
) => {
  console.log(
    'Getting transactions for account',
    iban,
    'from',
    fromDate,
    'to',
    toDate,
  );
  const exchangeRateBuy = await getNumber(
    browser,
    selectors.dashboard.exchangeRateBuy,
  );
  const exchangeRateSell = await getNumber(
    browser,
    selectors.dashboard.exchangeRateSell,
  );

  await filterByDate(browser, fromDate, toDate);
  await browser.waitForElementVisible(selectors.accountDetails.transactions);

  const transactions = await browser.findElements(
    selectors.accountDetails.transactions,
  );

  console.log('Found', transactions.length - 1, 'transactions');

  const transactionsList: Transaction[] = [];

  const firstTransaction: string = (await browser.getText(
    selectors.accountDetails.transaction,
  )) as any;

  if (
    transactions.length === 2 &&
    /No hay detalle de movimientos/i.test(firstTransaction)
  ) {
    return transactionsList;
  }

  for (let j = 1; j < transactions.length; j++) {
    const transaction: Partial<Transaction> = {
      index: j,
      account: iban,
      currency,
      exchangeRate: currency === 'USD' ? exchangeRateBuy : exchangeRateSell,
    };

    const transactionSelector = selectors.accountDetails.transaction.replace(
      '(2)',
      `(${j + 1})`,
    );

    transaction.ref = await getNumber(
      browser,
      `${transactionSelector} ${selectors.accountDetails.transactionRef}`,
    );

    transaction.date = await getDate(
      browser,
      `${transactionSelector} ${selectors.accountDetails.transactionDate}`,
    );
    transaction.detail = await getString(
      browser,
      `${transactionSelector} ${selectors.accountDetails.transactionName}`,
    );
    transaction.debit = await getNumber(
      browser,
      `${transactionSelector} ${selectors.accountDetails.transactionDebt}`,
    );
    transaction.credit = await getNumber(
      browser,
      `${transactionSelector} ${selectors.accountDetails.transactionCrdt}`,
    );
    transaction.balance = await getNumber(
      browser,
      `${transactionSelector} ${selectors.accountDetails.transactionBalance}`,
    );

    if (/^TEF A/.test(transaction.detail)) {
      await browser.click(
        `${transactionSelector} ${selectors.accountDetails.transactionRef} a`,
      );
      await browser.waitForElementVisible(
        selectors.transferDetails.transferDetailsContainer,
      );

      transaction.exchangeRate = await getNumber(
        browser,
        selectors.transferDetails.exchangeRate,
      );

      transaction.destinyAmount = await getNumber(
        browser,
        selectors.transferDetails.creeditAmount,
      );

      transaction.destinyCurrency = ((
        await getString(browser, selectors.transferDetails.creeditAmount)
      ).match(/[A-Z]{3}/) || ['UKN'])[0];

      if (transaction.exchangeRate === 1) {
        transaction.exchangeRate =
          transaction.destinyCurrency === 'USD'
            ? exchangeRateBuy
            : exchangeRateSell;
      }

      transaction.message = await getString(
        browser,
        selectors.transferDetails.description,
      );

      await browser.click(selectors.transferDetails.backButton);
      await browser.waitForElementVisible(
        selectors.accountDetails.advancedSearch,
      );
      await browser.click(selectors.accountDetails.advancedSearch);
      await filterByDate(browser, fromDate, toDate);
      await browser.waitForElementVisible(
        selectors.accountDetails.transactions,
      );
    }

    transactionsList.push(transaction as Transaction);
  }

  return transactionsList;
};

const getCreditCardTransactions = async (
  browser: NightwatchBrowser,
  { account }: { account: number },
  fromDate: Date,
  toDate: Date = fromDate,
) => {
  const transactionsList: Transaction[] = [];
  console.log('Getting transactions for account', account);
  const exchangeRateBuy = await getNumber(
    browser,
    selectors.dashboard.exchangeRateBuy,
  );
  const exchangeRateSell = await getNumber(
    browser,
    selectors.dashboard.exchangeRateSell,
  );

  await browser.waitForElementVisible(
    `${selectors.creditCardDetails.recentTransactionsTable} ${selectors.creditCardDetails.transactions}`,
  );

  const fromDateMoment = moment(fromDate);
  const isFromDateAfter15 = fromDateMoment.isAfter(moment().date(15));

  const isSameMonth = fromDateMoment.isSame(moment(), 'month');
  const isWithinCurrentRange =
    isSameMonth &&
    (moment().isAfter(moment().date(15)) ? isFromDateAfter15 : true);

  const selectMonth = async (monthName: string) => {
    console.log('Selecting month', monthName);

    await browser.click(selectors.creditCardDetails.monthSelect);
    const selectItems = await browser.findElements(
      selectors.creditCardDetails.monthSelectItems,
    );

    for (let i = 1; i < selectItems.length; i++) {
      const itemSelector = selectors.creditCardDetails.monthSelectItem.replace(
        '(2)',
        `(${i + 1})`,
      );
      const text = await browser.getText(itemSelector);

      if (text.toLowerCase().includes(monthName)) {
        console.log('Selecting item', text);
        await browser.click(itemSelector);
        await browser.pause(2000);
        break;
      }
    }
  };

  const getMonthTransactions = async (recent = true) => {
    const transactionsSelector = `${
      recent
        ? selectors.creditCardDetails.recentTransactionsTable
        : selectors.creditCardDetails.monthTransactionsTable
    } ${selectors.creditCardDetails.transactions}`;

    try {
      await browser.waitForElementVisible(transactionsSelector);
    } catch (e) {
      console.log('No transactions found for account', account);
      return transactionsList;
    }

    const transactions = await browser.findElements(transactionsSelector);

    if (transactions.length < 3) {
      console.log('No transactions found for account', account);
      return transactionsList;
    }

    console.log(
      'Found',
      transactions.length - 2,
      'transactions for account',
      account,
    );

    for (let j = 2; j < transactions.length; j++) {
      const transactionSelector =
        selectors.creditCardDetails.transaction.replace('(3)', `(${j + 1})`);

      const detail = await getString(
        browser,
        `${transactionSelector} ${selectors.creditCardDetails.transactionName}`,
      );

      const amountUSD = await getNumber(
        browser,
        `${transactionSelector} ${selectors.creditCardDetails.transactionDebtUSD}`,
      );

      const amountCRC = await getNumber(
        browser,
        `${transactionSelector} ${selectors.creditCardDetails.transactionDebtCRC}`,
      );

      const date = await getDate(
        browser,
        `${transactionSelector} ${selectors.creditCardDetails.transactionDate}`,
      );

      const dateTime = (date.getTime() / 1000).toString();
      let ref = parseInt(`${dateTime}0`, 10);
      const sameDateTxs = transactionsList.filter((t) =>
        t.ref.toString().startsWith(dateTime),
      );
      if (sameDateTxs.length) {
        ref = parseInt(dateTime + sameDateTxs.length, 10);
      }

      if (
        !(
          moment(date).isSameOrAfter(fromDate) &&
          moment(date).isSameOrBefore(toDate)
        )
      ) {
        continue;
      }

      if (amountUSD) {
        transactionsList.push({
          index: j,
          account,
          currency: 'USD',
          exchangeRate: exchangeRateBuy,
          date,
          ref,
          detail,
          ...(amountUSD > 0
            ? { debit: amountUSD }
            : { credit: Math.abs(amountUSD) }),
        } as Transaction);
      }

      if (amountCRC) {
        transactionsList.push({
          index: j,
          account,
          currency: 'CRC',
          exchangeRate: exchangeRateSell,
          date,
          ref,
          detail,
          ...(amountCRC > 0
            ? { debit: amountCRC }
            : { credit: Math.abs(amountCRC) }),
        } as Transaction);
      }
    }
  };

  if (!isSameMonth) {
    await selectMonth(fromDateMoment.locale('es').format('MMMM'));
  }

  // Get current month transactions
  await getMonthTransactions(isSameMonth);

  // Get previous month transactions
  await selectMonth(
    fromDateMoment.subtract(1, 'month').locale('es').format('MMMM'),
  );
  await getMonthTransactions(false);

  if (!transactionsList.length) {
    console.log(
      'No transactions found for account',
      account,
      'between dates',
      fromDate,
      toDate,
    );
  }

  return transactionsList;
};

export const bacWorker = async (browser: NightwatchBrowser) => {
  const transactions: Transaction[] = [];
  await login(browser);

  await browser.waitForElementVisible(selectors.dashboard.accounts);
  const accounts = await browser.findElements(selectors.dashboard.accounts);

  console.log('Processing accounts', accounts.length);
  for (let i = 1; i < accounts.length; i++) {
    const url: string = (await browser.url()) as any;

    const accountSelector = selectors.dashboard.account.replace(
      '(1)',
      `(${i})`,
    );

    const accountDetails = await getAccountDetails(browser, accountSelector);
    await navigateToAccountDetails(browser, accountSelector);

    const newTransactions = await getTransactions(
      browser,
      accountDetails,
      bacLastQueryDate,
      moment(bacLastQueryDate)
        .add(bacQueryDays - 1, 'days')
        .toDate(),
    );
    transactions.push(...newTransactions);

    await browser.url(url);
    await browser.waitForElementVisible(selectors.dashboard.accounts);
  }

  await browser.waitForElementVisible(selectors.dashboard.creditCards);
  const creditCards = await browser.findElements(
    selectors.dashboard.creditCards,
  );

  console.log('Processing credit cards', creditCards.length);
  for (let i = 1; i <= creditCards.length; i++) {
    const url: string = (await browser.url()) as any;

    const creditCardSelector = selectors.dashboard.creditCard.replace(
      '(1)',
      `(${i})`,
    );

    const creditCardDetails = await getCreditCardDetails(
      browser,
      creditCardSelector,
    );
    await navigateToCreditCardDetails(browser, creditCardSelector);

    const newTransactions = await getCreditCardTransactions(
      browser,
      {
        account: creditCardDetails.account,
      },
      bacLastQueryDate,
      moment(bacLastQueryDate)
        .add(bacQueryDays - 1, 'days')
        .toDate(),
    );
    transactions.push(...newTransactions);

    await browser.url(url);
    await browser.waitForElementVisible(selectors.dashboard.creditCards);
  }

  process.send(transactions);

  await logout(browser);
};
