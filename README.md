# Baranoz

Monorepo for Baranoz utilities projects

## Environment setup

- A classic K8S cluster hosted in Linux or Mac (I used [minikube](https://v1-18.docs.kubernetes.io/docs/tasks/tools/install-minikube/))
- [Istio](https://istio.io/latest/docs/setup/install/istioctl/) installed
- [Knative](https://knative.dev/docs/admin/install/knative-with-operators/)
- A public IP routing to the server

## Development setup

- [skaffold](https://skaffold.dev/docs/install/)
- [sops](https://github.com/jkroepke/helm-secrets#installation-and-dependencies)
- [helm](https://helm.sh/docs/intro/install/#through-package-managers)
- [helm-secrets](https://github.com/jkroepke/helm-secrets#using-helm-plugin-manager)

## Applications

- **[Notion Watchers](packages/notion-watchers/)** - Watchers and api interactions with the Notion API
