#!/bin/bash

source $(dirname "$0")/common.sh

set -e
set -x

kubectl create namespace $SKAFFOLD_NAMESPACE || true
kubectl create secret docker-registry gitlab-registry \
  -n $SKAFFOLD_NAMESPACE \
  --docker-server=$CI_REGISTRY \
  --docker-username=$GITLAB_USER \
  --docker-password=$GITLAB_TOKEN \
  --docker-email=devops@diegobarahona.com || true
skaffold deploy -p ${PIPELINE_DEPLOY_MODE:-local} --build-artifacts=tags.json
