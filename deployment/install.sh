#!/bin/bash

source $(dirname "$0")/common.sh

set -e

if [ "$PIPELINE_PUBLISH_MODE" = "prerelease" ] || [ "$PIPELINE_PUBLISH_MODE" = "release" ]; then
  git config --global user.name "$GITLAB_USER_NAME"
  git config --global user.email "$GITLAB_USER_EMAIL"
  git remote set-url origin "https://${GITLAB_USER}:${GITLAB_TOKEN}@$CI_SERVER_HOST/$CI_PROJECT_PATH.git"
  git checkout -B "$CI_COMMIT_REF_NAME" "$CI_COMMIT_SHA"
fi

npm config set @diestrin:registry "https://gitlab.com/api/v4/packages/npm/"
npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' "${CI_JOB_TOKEN}"
npm config set unsafe-perm true

set -x
npm ci --cache $CI_PROJECT_DIR/.npm --prefer-offline
npx lerna bootstrap
{ set +x; } 2>/dev/null

npm config set @diestrin:registry "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/"
npm config set "//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken" "${CI_JOB_TOKEN}"

set -x
npm run publish:$PIPELINE_PUBLISH_MODE
