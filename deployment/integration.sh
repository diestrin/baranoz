#!/bin/bash

source $(dirname "$0")/common.sh

set -e

helm test --logs --namespace $SKAFFOLD_NAMESPACE $HELM_CHART_NAME
