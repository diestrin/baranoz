#!/bin/bash

source $(dirname "$0")/common.sh

set -e
set -x

export NPM_TOKEN=$CI_JOB_TOKEN
export NOTION_WATCHERS_VERSION=$(jq -r '.version' ./packages/notion-watchers/package.json)

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

skaffold build -p ${PIPELINE_BUILD_MODE:-local} --file-output=tags.json
